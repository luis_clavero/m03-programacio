import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("User ID: ");
		int userId = sc.nextInt();
		
		try {
			if (userId != 1234) {
				throw new InvalidUserException();
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		
		int a = sc.nextInt();
		int b = sc.nextInt();
	}
}
