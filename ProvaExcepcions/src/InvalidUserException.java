
public class InvalidUserException extends Exception{
	
	@Override
	public String toString() {
		return ("Invalid user id entered");
	}
}
