package exercicis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Exercici3 {

	public static void main(String[] args) throws IOException {
		File file1 = new File("src/exercicis/fitxer1.txt");
		FileReader fr1 = null;
		BufferedReader br1;
		String line1;
		
		File file2 = new File("src/exercicis/fitxer2.txt");
		FileReader fr2 = null;
		BufferedReader br2;
		String line2;
		
		int currentLineNumber = 0;
		int differingLineNumber;
		
		try {
			file1.mkdir();
			fr1 = new FileReader(file1);
			br1 = new BufferedReader(fr1);
			
			fr2 = new FileReader(file2);
			br2 = new BufferedReader(fr2);
			
			while ((line1 = br1.readLine()) != null && (line2 = br2.readLine()) != null) {
				currentLineNumber++;
				if (!line1.equals(line2)) {
					differingLineNumber = currentLineNumber;
					System.out.println("La línia que difereix és la número: " + differingLineNumber);
					System.out.println(file1.getName() + ": " + line1);
					System.out.println(file2.getName() +  ": " + line2);
				}
			}
			
		} catch (FileNotFoundException e) {
			System.out.println("Fitxer no trobat.");
		} catch (IOException e) {
			System.out.println(e.getMessage());
		} finally {
			if (fr1 != null && fr2 != null) {
				fr1.close();
				fr2.close();
			}
		}
	}

}
