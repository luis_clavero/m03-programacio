package exercicis;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exercici8 {

	public static void main(String[] args) throws IOException {
		findLinks("http://www.escoladeltreball.org/ca");
	}

	public static void findLinks(String urlInString) throws IOException {
		String line, link;
		Matcher m;
		// Create the url connection
		URL url = new URL(urlInString);
		URLConnection uc = url.openConnection();
		// Create a buffered reader to read from the file
		BufferedReader br = new BufferedReader(new InputStreamReader(
				uc.getInputStream()));
		// Create the writers to print the links into the file
		FileWriter fw = new FileWriter("src/exercicis/links.txt", true);
		PrintWriter pw = new PrintWriter(fw, true);
		// Create the regex matcher of links
		Pattern p = Pattern.compile("href=\"(.*?)\"");
		// Iterate the whole file
		while ((line = br.readLine()) != null) {
			// Assign the matcher the part of text
			m = p.matcher(line);
			// If the text matches the pattern get the url of the link
			if (m.find()) {
				link = m.group(1);
				// Print the link into the file
				pw.println(link);
			}
		}
		// Close the readers and writers
		br.close();
		fw.close();
	}
}
