package exercicis;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;

public class Exercici10 {

	public static void main(String[] args) throws IOException {
		// Apartat 1

		File apartat1 = new File("directoriExercici10");
		FileUtils.forceMkdir(apartat1);
		File apartat1Origen = new File(apartat1.getAbsolutePath() + "/origen");
		FileUtils.forceMkdir(apartat1Origen);

		// Apartat 2

		File apartat2;
		for (int i = 1; i < 6; i++) {
			apartat2 = new File("directoriExercici10/fitxer" + i + ".txt");
			System.out.println(
					"APARTAT 2: S'ha creat el fitxer " + apartat2.getCanonicalPath() + "? " + apartat2.createNewFile());
		}
		System.out.println();

		// Apartat 3

		List<File> files = Arrays.asList(apartat1.listFiles());
		for (File file : files) {
			if (!file.isDirectory()) {
				FileUtils.writeLines(file, files);
			}
		}

		// Apartat 4

		for (File file : files) {
			if (!file.isDirectory()) {
				FileUtils.moveFileToDirectory(file, apartat1Origen, false);
			}
		}
		System.out.println("APARTAT 4: S'HAN MOGUT ELS 5 FITXERS A " + apartat1Origen.getCanonicalPath());
		System.out.println();

		// Apartat 5

		System.out.println("APARTAT 5: Està fitxer3.txt?: " + FileUtils.directoryContains(apartat1Origen,
				FileUtils.getFile("directoriExercici10/origen/fitxer3.txt")));
		System.out.println();
		
		// Apartat 6

		FileUtils.forceDelete(FileUtils.getFile("directoriExercici10/origen/fitxer3.txt"));
		System.out.println("APARTAT 6: Està fitxer3.txt?: " + FileUtils.directoryContains(apartat1Origen,
				FileUtils.getFile("directoriExercici10/origen/fitxer3.txt")));
		System.out.println();
		
		// Apartat 7

		System.out.println("APARTAT 7: ");
		System.out.println("Útilma modificació fitxer1.txt:" + FileUtils.getFile("directoriExercici10/origen/fitxer1.txt").lastModified());
		System.out.println("Útilma modificació fitxer5.txt:" + FileUtils.getFile("directoriExercici10/origen/fitxer5.txt").lastModified());
		System.out.println("Fitxer1.txt eś més vell que fitxer5.txt?: "
				+ FileUtils.isFileOlder(FileUtils.getFile("directoriExercici10/origen/fitxer1.txt"),
						FileUtils.getFile("directoriExercici10/origen/fitxer5.txt").lastModified()));
		System.out.println();
		
		// Apartat 8
		
		FileUtils.writeStringToFile(FileUtils.getFile("directoriExercici10/origen/fitxer1.txt"), "linia sobreescrita");
		System.out.println("APARTAT 8: CANVIAT EL CONTINGUT DEL FITXER fitxer1.txt");
		System.out.println();
		
		// Apartat 9
		
		System.out.println("APARTAT 9: ");
		System.out.println("Útilma modificació fitxer1.txt:" + FileUtils.getFile("directoriExercici10/origen/fitxer1.txt").lastModified());
		System.out.println("Útilma modificació fitxer5.txt:" + FileUtils.getFile("directoriExercici10/origen/fitxer5.txt").lastModified());
		System.out.println("Fitxer1.txt eś més nou que fitxer5.txt?: "
				+ FileUtils.isFileOlder(FileUtils.getFile("directoriExercici10/origen/fitxer5.txt"),
						FileUtils.getFile("directoriExercici10/origen/fitxer1.txt").lastModified()));
		System.out.println();
		
		// Apartat 10
		
		FileUtils.touch(FileUtils.getFile("directoriExercici10/origen/fitxer5.txt"));
		System.out.println("APARTAT 10: ");
		System.out.println("Útilma modificació fitxer1.txt:" + FileUtils.getFile("directoriExercici10/origen/fitxer1.txt").lastModified());
		System.out.println("Útilma modificació fitxer5.txt:" + FileUtils.getFile("directoriExercici10/origen/fitxer5.txt").lastModified());
		System.out.println("Fitxer1.txt eś més nou que fitxer5.txt?: "
				+ FileUtils.isFileOlder(FileUtils.getFile("directoriExercici10/origen/fitxer5.txt"),
						FileUtils.getFile("directoriExercici10/origen/fitxer1.txt").lastModified()));
		System.out.println();
		
		// Apartat 11
		
		File apartat11 = new File(apartat1.getAbsoluteFile() + "/desti");
		FileUtils.moveDirectoryToDirectory(apartat1Origen, apartat11, true);
		System.out.println("APARTAT 11: S'HA MOGUT EL DIRECTORI " + apartat1Origen.getCanonicalPath() + " A DINS DEL DIRECTORI " + apartat11.getCanonicalPath());
		System.out.println();
		
		// Apartat 12
		
		BigInteger size = FileUtils.sizeOfDirectoryAsBigInteger(apartat11);
		System.out.println("APARTAT 12: EL TAMANY DEL DIRECTORI " + apartat11.getName() + " ES: " + size);
		System.out.println();
		
		// Apartat 13
		
		// No sabia fer-lo
	}

}
