package exercicis;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exercici9 {

	public static void main(String[] args) {
		ArrayList<String> files = new ArrayList<String>();
		files =	mostrarFitxersDirectoris(files, "/home/Luis/Documents/M03-Programacio/ExerciciStreams");
		for (String s : files) {
			System.out.println(s);
		}

	}

	public static ArrayList<String> mostrarFitxersDirectoris(ArrayList<String> listOfFiles, String filename) {
		// Create the file object
		File initialDirectory = new File(filename);
		// ArrayList of the names
		// Get the list of files in the directory
		List<File> files = Arrays.asList(initialDirectory.listFiles());
		// Iterate the list of the files
		for (File file : files) {
			// If it's a file add it to the list, call the function recursively otherwise
			// f for file d for directory
			if (file.isFile()) {
				listOfFiles.add("f : " + file.getAbsolutePath());
			} else {
				listOfFiles.add("d : " + file.getAbsolutePath());
				mostrarFitxersDirectoris(listOfFiles, file.getAbsolutePath());
			}
		}
		return listOfFiles;
	}
}
