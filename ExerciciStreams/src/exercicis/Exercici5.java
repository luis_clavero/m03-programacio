package exercicis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Exercici5 {

	public static void main(String[] args) throws IOException {
		File directory = new File("src/exercicis");
		List<String> filesInDirectory = new ArrayList<String>();
		filesInDirectory = Arrays.asList(directory.list());
		countLinesChars(filesInDirectory);
	}

	public static void countLinesChars(List<String> filesInDirectory) throws IOException {
		FileReader fr = null;
		BufferedReader br;
		int lines = 0;
		int characters = 0;
		String line;
		
		// Loop the files
		for (String fileName : filesInDirectory) {
			try {
				// Try to create the reader and the buffered reader
				fr = new FileReader(fileName);
				br = new BufferedReader(fr);
				// Count the lines and characters of each file
				while((line = br.readLine()) != null) {
					line = line.replaceAll(" ", "");
					lines++;
					characters += line.length();
				}
				System.out.println(fileName + ": " + lines + " línies i " + characters + " caràcters.");
				lines = 0;
				characters = 0;
			} catch (IOException e) {
				System.out.println(e.getMessage());
			} finally {
				if (fr != null) {
					fr.close();
				}
			}
		}
	}
}
