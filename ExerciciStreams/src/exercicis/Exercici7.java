package exercicis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class Exercici7 {

	public static void main(String[] args) throws IOException {
		File file = new File("src/exercicis/Exercici7.java");
		countChars(file);
	}

	public static void countChars(File file) throws IOException {
		String line;
		int totalChars = 0;
		int currentRepetitions;
		char currentChar;
		HashMap<Character, Integer> repetitions = new HashMap<Character, Integer>();
		FileReader fr = new FileReader(file);
		BufferedReader br = new BufferedReader(fr);
		// Loop each line of the file
		while ((line = br.readLine()) != null) {
			for (int i = 0; i < line.length(); i++) {
				// Get the current character
				currentChar = line.charAt(i);
				// If the character does not exist the repetitions of the character are 0
				if (repetitions.get(currentChar) == null) {
					currentRepetitions = 0;
				} else {
					currentRepetitions = repetitions.get(currentChar);
				}
				repetitions.put(currentChar, currentRepetitions + 1);
			}
			totalChars += line.length();
		}
		// Close the file reader
		fr.close();
		// Create the new file to print the information of the information about the characters
		FileWriter fw = new FileWriter("src/exercicis/RecompteCaracters.txt", true);
		PrintWriter pw = new PrintWriter(fw, true);
		// Iterate the hash map
		for (Character key : repetitions.keySet()) {
			// Print the info into the file
		    pw.println("\"" + key + "\"\t" + repetitions.get(key) + "\t" + ((double)repetitions.get(key) / totalChars * 100) + "%");
		}
		// Close the writer
		fw.close();
	}
}
