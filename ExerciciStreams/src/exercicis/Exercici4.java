package exercicis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Exercici4 {

	public static void main(String[] args) throws IOException {
		// The path of the directory
		File directory = new File("src/exercicis");
		// The list of the files and subdirectories in the directory
		File[] fileDirList = directory.listFiles();
		String backupDirectoryPath = createBackupName(directory);
		copyFilesIntoDirectory(fileDirList, backupDirectoryPath);
	}

	public static String createBackupName(File directory) {
		int backupNumber = 0;
		File backupDirectory = new File(directory.getAbsolutePath() + "/" + "backUp" + backupNumber);
		// While the backupDirectory exists we have to change its name
		while (backupDirectory.exists()) {
			backupNumber++;
			backupDirectory.renameTo(new File(directory.getAbsolutePath() + "/" + "backUp" + backupNumber));
		}
		// Create the directory
		backupDirectory.mkdir();
		return backupDirectory.getAbsolutePath();
	}
	
	public static void copyFilesIntoDirectory(File[] fileDirList, String directoryPath) throws IOException {
		String line;
		// Loop all the files in the list
		for (File file : fileDirList) {
			// Check the file is a file and we have access to read it
			if (file.canRead()) {
				if (file.isFile()) {
					// Create a new file in the backup directory
					File backupFile = new File(directoryPath + "/" + file.getName());
					FileReader fr = new FileReader(file);
					BufferedReader br = new BufferedReader(fr);
					
					// Create the writer to write in the backupfile
					FileWriter fw = new FileWriter(backupFile, true);
					PrintWriter pw = new PrintWriter(fw, true);
					while ((line = br.readLine()) != null) {
						pw.println(line);
					}
					// Close the file reader and the writer and create the file 
					fr.close();
					fw.close();
					backupFile.createNewFile();
				}
			} else {
				System.out.println("El fitxer " + file.getAbsolutePath() + " no és pot llegir");
			}
		}
	}
}
