package practica3;

public class Tech extends Employee {

	// Attributes
	public double hourlyWage;
	public int hoursWorked;

	// Constructor
	public Tech(String id, String name, int age, double hourlyWage, int hoursWorked) {
		super();
		this.setData(id, name, age);
		this.hourlyWage = hourlyWage;
		this.hoursWorked = hoursWorked;
	}

	// Methods
	
	/**
	 * Get the salary of the the employee.
	 */
	public double getSalary() {
		return hourlyWage * hoursWorked;
	}

	@Override
	public String toString() {
		return this.getData() + "\nHourly Wage: " + this.hourlyWage + "\nHours Worked: " + this.hoursWorked
				+ "\nSalary: " + this.getSalary();
	}
}
