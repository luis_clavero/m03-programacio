package practica3;

public abstract class Employee {

	// Employee attributes
	public String id;
	public String name;
	public int age;

	// Constructor
	public Employee() {
		this.id = "";
		this.name = "";
		this.age = -1;
	}

	// Methods

	/**
	 * Set the data of the employee
	 * 
	 * @param id,   the employer's id
	 * @param name, the employer's name
	 * @param age,  the employer's age
	 * @return void
	 */
	public void setData(String id, String name, int age) {
		this.id = id;
		this.name = name;
		this.age = age;
	}

	/**
	 * Get the data of the employee
	 * 
	 * @return all the data of the employee
	 */
	public String getData() {
		return "ID: " + this.id + "\nName: " + this.name + "\nAge: " + this.age;
	}

	/**
	 * Get the salary of each employee
	 */
	public abstract double getSalary();

	/**
	 * toString methods for the class
	 * 
	 * @return the data of the object
	 */
	@Override
	public String toString() {
		return "Employee:\n" + this.getData();
	}

}
