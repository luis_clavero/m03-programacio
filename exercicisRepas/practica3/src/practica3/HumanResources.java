package practica3;

public class HumanResources extends Employee {

	// Attributes
	public double monthlyWage;
	public int monthsWorked;

	// Constructor
	public HumanResources(String id, String name, int age, double monthlyWage, int monthsWorked) {
		super();
		this.setData(id, name, age);
		this.monthlyWage = monthlyWage;
		this.monthsWorked = monthsWorked;
	}

	// Methods
	
	/**
	 * Get the salary of the the employee.
	 */
	public double getSalary() {
		return monthlyWage * monthsWorked;
	}

	@Override
	public String toString() {
		return this.getData() + "\nMonthly Wage: " + this.monthlyWage + "\nMonths Worked: " + this.monthsWorked
				+ "\nSalary: " + this.getSalary();
	}
}
