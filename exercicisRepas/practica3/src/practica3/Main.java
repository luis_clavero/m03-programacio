package practica3;

public class Main {

	public static void main(String[] args) {
		
		Tech t = new Tech("1", "Luis", 19, 12.5, 350);
		Design d = new Design("2", "Mario", 28, 1105.50, 16);
		HumanResources hr = new HumanResources("3", "Jordi", 43, 6000, 8);
		System.out.println(t + "\n");
		System.out.println(d + "\n");
		System.out.println(hr);
	}

}
