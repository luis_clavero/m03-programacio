package practica3;

public class Design extends Employee {

	// Attributes
	public double weeklyWage;
	public int weeksWorked;

	// Constructor
	public Design(String id, String name, int age, double weeklyWage, int weeksWorked) {
		super();
		this.setData(id, name, age);
		this.weeklyWage = weeklyWage;
		this.weeksWorked = weeksWorked;
	}

	// Methods
	
	/**
	 * Get the salary of the the employee.
	 */
	public double getSalary() {
		return weeklyWage * weeksWorked;
	}

	@Override
	public String toString() {
		return this.getData() + "\nWeekly Wage: " + this.weeklyWage + "\nWeeks Worked: " + this.weeksWorked
				+ "\nSalary: " + this.getSalary();
	}
}
