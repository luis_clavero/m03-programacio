package practica2;

public class Circle {

	// Circle attributes
	public double radius;
	
	// Constructor
	public Circle(double radius) {
		this.radius = radius;
	}
	
	// Methods
	
	/**
	 * Get the circl'es perimeter
	 * 
	 * @return perimeter the circle's perimeter
	 */
	public double getPerimeter() {
		double perimeter;
		perimeter = 2 * Math.PI * this.radius;
		return perimeter;
	}
	
	/**
	 * Get the circle's area
	 * 
	 * @return area the circle's area
	 */
	public double getArea() {
		double area;
		area = Math.PI * Math.pow(this.radius, 2);
		return area;
	}

	/**
	 * toString for this class;
	 */
	@Override
	public String toString() {
		return "Circle [radius=" + radius + ", getPerimeter()=" + getPerimeter() + ", getArea()=" + getArea() + "]";
	}
}
