package practica1;

public class Programmer {

	// Programmer attributes
	public String name;
	public String id;
	public int salary;
	
	// Constructor
	
	public Programmer(String name, String id, int salary) {
		this.name = name;
		this.id = id;
		this.salary = salary;
	}

	// Getters and setters
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public int getSalary() {
		return salary;
	}

	public void setSalary(int salary) {
		this.salary = salary;
	}
	
	// Methods 
	
	@Override
	public String toString() {
		return "Programmer [name=" + name + ", id=" + id + ", salary=" + salary + "]";
	}
	
	
}
