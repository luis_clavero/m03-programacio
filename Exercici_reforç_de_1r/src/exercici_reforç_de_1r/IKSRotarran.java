package exercici_reforç_de_1r;

import java.time.LocalDateTime;

public class IKSRotarran {

	public static void main(String[] args) {
		LocalDateTime ld_01 =  LocalDateTime.parse("1954-08-15T00:01"); 
		Oficial capita = new Oficial("001-A", "Martok", true, ld_01, 1, 1, "Capitanejar la nau.");
		
		LocalDateTime ld_02 =  LocalDateTime.parse("1981-12-26T13:42");
		Mariner mariner_02_03 = new Mariner("758-J", "Kurak", true, ld_02, 3, 1, "Mariner encarregat del tim� i la navegaci� durant el 2ntorn.");
		
		System.out.println("Departament del capita: " + capita.departament); // Podem accedir degut a que es protected 
		// System.out.println("Descripció de la feina: " + capita.descripcioFeina); No podem accedir degut a que es privat
		System.out.println("Descripci� de la feina: " + capita.getDescripcioFeina()); // Accedim amb el getter
		System.out.println();
		capita.ImprimirDadesTripulant();
		capita.departament = 10;
		
		Oficial oficialDeTipusOficial = capita;
		Tripulant oficialDeTipusTripulant = capita;
		
		System.out.print("\nOficial: "); 
		oficialDeTipusOficial.saludar();
		System.out.println();		// Els dos saludar() criden al de la subclasse
		System.out.print("Tripulant: ");
		oficialDeTipusTripulant.saludar();
		
		System.out.println();
		System.out.println("L'objecte capita (té implementat el toString()): " + capita);
		System.out.println("L'objecte mariner_02_03 (NO té implementat el toString() ): " + mariner_02_03);
		
		System.out.println();
		mariner_02_03.ImprimirDadesTripulant();
	}
	
	/**
	 * Format a date to String 'DD-MM-YYYY hh:mm'
	 * @param date
	 * @return the date in a string
	 */
	public static String dateToString(LocalDateTime date) {
		String formatedDate = "";
		formatedDate += date.getDayOfMonth() + "-" + date.getMonthValue() + "-" + date.getYear() +
				" " + date.getHour() + date.getMinute();
		return formatedDate;
	}

}
