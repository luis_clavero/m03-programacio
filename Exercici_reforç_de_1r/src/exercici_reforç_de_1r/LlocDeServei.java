package exercici_refor�_de_1r;

public class LlocDeServei {
	
	// LlocDeServei attributes
	
	private int id;
	private String nom;
	private String descripcio;
	
	// Constructor
	
	public LlocDeServei(int id, String nom, String descripcio) {
		super();
		this.id = id;
		this.nom = nom;
		this.descripcio = descripcio;
	}
	
	// Getters & Setters

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDescripcio() {
		return descripcio;
	}

	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}

}
