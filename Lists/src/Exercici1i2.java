import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import java.util.Iterator;

public class Exercici1i2 {

	public static void main(String[] args) {
		
		int i, j;
		List<Integer> list1 = new ArrayList<Integer>(); 
		
		for (i = 0; i < args.length; i++) {
			list1.add(Integer.parseInt(args[i]));
		}
		
		System.out.println(list1);
		System.out.println("Numeros afegits: " + list1.size());
		
		for (i = 0; i < args.length; i++) {
			list1.set(i, (int)Math.pow(list1.get(i), 2));
		}
		
		Iterator<Integer> it = list1.iterator();
		
		while (it.hasNext()) {
			if (it.next() > 100) {
				it.remove();
			}
		}
		
		Collections.sort(list1);
		System.out.println(list1);
		
		for (Integer num : list1) {
			System.out.println(num);
		}
		
		List<Integer> list2 = new ArrayList<Integer>();
		list2.add(24);
		list2.add(34);
		list2.add(8);
		list2.add(11);
		list2.add(56);
		
		list1.addAll(list2);
		
		for (Integer num : list2) {
			System.out.println(list1.contains(num));
		}
		
		list2.clear();
		System.out.println(list2);
	}

}
