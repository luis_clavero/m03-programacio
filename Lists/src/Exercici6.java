import java.util.ArrayList;
import java.util.Collections;

public class Exercici6 {

	public static void main(String[] args) {
		
		ArrayList<EntradaAgenda> agenda = new ArrayList<EntradaAgenda>();
		
		EntradaAgenda p1 = new EntradaAgenda("Luis", "Clavero", "Carrillo");
		EntradaAgenda p2 = new EntradaAgenda("Nil", "Gomis", "Sola");
		EntradaAgenda p3 = new EntradaAgenda("Mario", "Alvarez", "Sierra");
		EntradaAgenda p4 = new EntradaAgenda("Martin", "Renna", "Jurado");
		EntradaAgenda p5 = new EntradaAgenda("Bruce", "Gomis", "Sola");
		
		agenda.add(p1);
		agenda.add(p2);
		agenda.add(p3);
		agenda.add(p4);
		agenda.add(p5);
		
		System.out.println(agenda);
		
		Collections.sort(agenda);

		System.out.println(agenda);
	}
}
