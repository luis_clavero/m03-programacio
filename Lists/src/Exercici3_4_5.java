import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;


public class Exercici3_4_5 {

	public static ArrayList<Person> minors(ArrayList<Person> people) {
		long age;
		for (int i = 0; i < people.size(); i++) {
			age = people.get(i).getBirthAge().getTime() / 1000 / 3600 / 24 / 365;
			if (age >= 18) {
				people.remove(i);
			}
		}
		return people ;
	}
	
	/*public static ArrayList<Person> orderPerson(ArrayList<Person> people) {
		int i, j;
		boolean swapped;
		Person person1, person2, toChangePerson;
		for (i = 0; i < people.size(); i++) {
			swapped = false;
			for (j = 0; j < people.size() - 1; j++) {
				person1 = people.get(j);
				person2 = people.get(j + 1);
				toChangePerson = person1.compareTo(person2);
				if (toChangePerson.equals(person1)) {
					people.set(j, person2);
					people.set(j + 1, person1);
					swapped = true;
				}
			}
			if (!swapped) {
				break;
			}
		}
		return people;
	}*/
	
	public static void main(String[] args) {
		
		Date d1 = new Date(2003, 3, 17);
		Date d2 = new Date(2002, 3, 17);
		Date d3 = new Date(2001, 3, 17);
		Date d4 = new Date(2000, 3, 17);
		
		Person p1 = new Person("Luis", "Clavero Carrillo", d1, "dni");
		Person p2 = new Person("Mario", "Alvarez Garcia", d1, "dni");
		Person p3 = new Person("Juan", "Muñoz Perez", d1, "dni");
		Person p4 = new Person("Alex", "Vicente Suarez", d1, "dni");
		
		ArrayList<Person> people = new ArrayList<Person>(); 
		people.add(p4);
		people.add(p2);
		people.add(p3);
		people.add(p1);
		
		System.out.println(people);
		
		Collections.sort(people);
		
		System.out.println(people);
	}

}
