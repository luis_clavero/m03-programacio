import java.util.Comparator;
import java.util.Date;

public class Person implements Comparable<Person> {
	
	private String name;
	private String surnames;
	private Date birthAge;
	private String dni;
	
	public Person(String name, String surnames, Date birthAge, String dni) {
		super();
		this.name = name;
		this.surnames = surnames;
		this.birthAge = birthAge;
		this.dni = dni;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurnames() {
		return surnames;
	}

	public void setSurnames(String surnames) {
		this.surnames = surnames;
	}

	public Date getBirthAge() {
		return birthAge;
	}

	public void setBirthAge(Date birthAge) {
		this.birthAge = birthAge;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birthAge == null) ? 0 : birthAge.hashCode());
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((surnames == null) ? 0 : surnames.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (birthAge == null) {
			if (other.birthAge != null)
				return false;
		} else if (!birthAge.equals(other.birthAge))
			return false;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (surnames == null) {
			if (other.surnames != null)
				return false;
		} else if (!surnames.equals(other.surnames))
			return false;
		return true;
	}

	

	@Override
	public String toString() {
		return "Person [name=" + name + ", surnames=" + surnames + "]";
	}

	@Override
	public int compareTo(Person p) {
		// Get the age in miliseconds
		long age1 = this.birthAge.getTime();
		long age2 = p.birthAge.getTime();
		int difference = (int)(age1 - age2);
		if (difference == 0) {
			difference = this.surnames.compareTo(p.getSurnames());
		}
		return difference;
	}
}
