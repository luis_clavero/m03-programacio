import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

public class Exercici7 {
	
	public static List<String> distributeCards(int numOfCards, List<String> deck) {
		Collections.shuffle(deck);
		int startingIndex = deck.size() - numOfCards;
		int endingIndex = deck.size();
		List<String> subList = deck.subList(startingIndex, endingIndex);
		List<String> hand = new ArrayList<String>(subList);
		deck.subList(startingIndex, endingIndex).clear();
		return hand;
	}
	
	public static void distributeCardsToPlayers(int numOfPlayers, int numOfCards, List<String> deck) {
		int i;
		List<String> playerHand;
		for (i = 0; i < numOfPlayers; i++) {
			playerHand = Exercici7.distributeCards(numOfCards, deck);
			System.out.println("Mà del jugador " + i + ": " + playerHand);
		}
	}

	public static void main(String[] args) {
		
		List<String> deck = new ArrayList<String>();
		String[] suits = {"Piques", "Copes", "Diamants", "Trèvols"};
		String[] cards = {"As", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"};
		String fullCard;
		int i,j;
		
		for (i = 0; i < suits.length; i++) {
			for (j = 0; j < cards.length; j++) {
				fullCard = suits[i] + cards[j];
				deck.add(fullCard);
			}
		}
		
		System.out.println(deck);
		System.out.println();
		Exercici7.distributeCardsToPlayers(4, 10, deck);
		System.out.println();
		System.out.println(deck);
	}

}
