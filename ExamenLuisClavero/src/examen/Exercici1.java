package examen;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.TrueFileFilter;



public class Exercici1 {
	
	static File patata = new File("fitxers_examen/patata/");
	static File index = new File(patata.getAbsolutePath() + "/index.txt");

	public static void main(String[] args) throws IOException {
		// Check if the file "patata" exists
		if (patata.exists()) { 
			System.out.println("el directori patata existeix.");
		} else {
			// Create the directory
			FileUtils.forceMkdir(patata);
			System.out.println("S'ha creat el fitxer: " + patata.getName());
			// Create the file index.txt
			index.createNewFile();
			System.out.println();
			System.out.println("S'ha creat el fitxer: " + index.getName());
			// Check if "index.txt" exists
			if (index.exists()) {
				System.out.println();
				System.out.println("El fitxer: " + index.getName() + " existeix.");
			} else {
				System.out.println();
				System.out.println("El fitxer: " + index.getName() + " no existeix.");
			}
			// Write 2 lines of text in "index.txt" file
			String lines = "Hola\nAdeu\n";
			FileUtils.writeStringToFile(index, lines);
			// Copy "dirACopiar" files to "patata"
			System.out.println();
			File dirACopiar = new File("fitxers_examen/dirACopiar");
			FileUtils.copyDirectory(dirACopiar, patata);
			// Copy the file "index.txt" into "fitxers_examen"
			File fitxersExamen = new File("fitxers_examen");
			FileUtils.copyFile(index, new File(fitxersExamen.getAbsolutePath() + "/index.txt"));
			// List all the files in the directory "patata"
			FileWriter fw = null;        
			PrintWriter pw = null;        
			StringBuilder permisosFile = new StringBuilder("---, ");        
			File fitxerIndex2;
			boolean append = true;
			fw = new FileWriter(new File(fitxersExamen.getAbsolutePath() + "/index.txt"), append);
			pw = new PrintWriter(fw, true);
			Iterator it = FileUtils.iterateFilesAndDirs(patata, TrueFileFilter.INSTANCE, TrueFileFilter.INSTANCE);
			// Segons l'ajuda el TrueFileFilter.INSTANCE determina tots els fitxers i directoris.
			while (it.hasNext()) {                    
				File fileTmp = (File) it.next();                    
				System.out.println(fileTmp.getName());
				if (fileTmp.canRead()) permisosFile.setCharAt(0, 'r');
				if (fileTmp.canWrite()) permisosFile.setCharAt(1, 'w');
				if (fileTmp.canExecute()) permisosFile.setCharAt(2, 'x');
				if (fileTmp.isHidden()){
					permisosFile.append("OCULT, ");                    
				}
				permisosFile.append("tamany = ");
				permisosFile.append(FileUtils.sizeOf(fileTmp));permisosFile.append(" bytes.");
				if (fileTmp.isDirectory()) {
					copiarNomEnFitxer(fileTmp.getName(), 0, permisosFile, pw);                    
				} else {
					copiarNomEnFitxer(fileTmp.getName(), 1, permisosFile, pw);      
				}
				permisosFile = new StringBuilder("---, ");
			}
			fw.close();
			// Delete the content of the "patata" directory
			FileUtils.cleanDirectory(patata);
			// Copy the file "index.txt" in the patata directory
			FileUtils.copyFile(new File(fitxersExamen.getAbsolutePath() + "/index.txt"), index);
			// Check if the 2 files have the same content
			boolean checkIfAreEqual = FileUtils.contentEquals(index, new File(fitxersExamen.getAbsolutePath() + "/index.txt"));
			System.out.println("\nEl fitxers index són iguals: " + checkIfAreEqual);
		}
	}
	
	private static void copiarNomEnFitxer(String nomFileTrobat, int tipusFile, StringBuilder permisosFile, PrintWriter pw) {
		pw.println(nomFileTrobat + "\t" + tipusFile + "\t" + permisosFile);
	}    
}
