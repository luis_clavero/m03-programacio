package com.daw.eva.ecom;


import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

/**
 *
 * @author eva
 * TODO
 * Cal definir els contractes de la interfície ClientDAO i demés entitats, i implementar-les 
 * Cal crear de la mateixa manera, ProducteJDBCDAO que implementa ProducteDAO, i idem per a ComandaJDBCDAO
 */
public class ProductJDBCDAO implements ClientDAO {

    // Conexión a la base de datos
    private static Connection conn = null;

    // Configuración de la conexión a la base de datos
    private static final String DB_HOST = "localhost";
    private static final String DB_PORT = "3306";
    private static final String DB_NAME = "tienda";
    private static final String DB_URL = "jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "?serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "Kaincc2127";
    private static final String DB_MSQ_CONN_OK = "CONEXIÓN CORRECTA";
    private static final String DB_MSQ_CONN_NO = "ERROR EN LA CONEXIÓN";

    //////////////////////////////////////////////////
    // MÉTODOS DE CONEXIÓN A LA BASE DE DATOS
    //////////////////////////////////////////////////
    ;
    
    /**
     * Intenta cargar el JDBC driver.
     * @return true si pudo cargar el driver, false en caso contrario
     */
    public static boolean loadDriver() {
        try {
            System.out.print("Loading Driver...");
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            System.out.println("OK!");
            return true;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Intenta conectar con la base de datos.
     *
     * @return true si pudo conectarse, false en caso contrario
     */
    public static boolean connect() {
    	// Establish the connection
        try {
        	conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	System.out.println(DB_MSQ_CONN_OK);
        	return true;
        } catch (Exception e) {
        	System.out.println(DB_MSQ_CONN_NO);
        	System.out.println(e);
        	return false;
        }
    }

    /**
     * Comprueba la conexión y muestra su estado por pantalla
     *
     * @return true si la conexión existe y es válida, false en caso contrario
     */
    public static boolean isConnected() {
        // Check if the connection is valid and it's not closed
        try {
        	if (conn.isValid(10) && !conn.isClosed()) {
        		return true;
        	}
        } catch (SQLException e) {
        	e.printStackTrace();
        }
        return false;
    }

    /**
     * Cierra la conexión con la base de datos
     */
    public static void close() {
        try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE TABLA producto
    //////////////////////////////////////////////////
    ;
    
    // Devuelve 
    // Los argumentos indican el tipo de ResultSet deseado
    /**
     * Obtiene toda la tabla producto de la base de datos
     * @param resultSetType Tipo de ResultSet
     * @param resultSetConcurrency Concurrencia del ResultSet
     * @return ResultSet (del tipo indicado) con la tabla, null en caso de error
     */
    public static ResultSet getTablaProducto(int resultSetType, int resultSetConcurrency) {
    	// Get the statement of the connection
    	Statement st;
		try {
			st = conn.createStatement(resultSetType, resultSetConcurrency);
	    	// Create the sql query
	    	String sql = "SELECT * FROM producto";
	    	// Execute the query
	    	ResultSet rs = st.executeQuery(sql);
	    	// Return the ResultSet
	    	return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
    }

    /**
     * Obtiene toda la tabla producto de la base de datos
     *
     * @return ResultSet (por defecto) con la tabla, null en caso de error
     */
    public static ResultSet getTablaProducto() {
    	// Get the statement of the connection
    	Statement st;
		try {
			st = conn.createStatement();
	    	// Create the sql query
	    	String sql = "SELECT * FROM producto";
	    	// Execute the query
	    	ResultSet rs = st.executeQuery(sql);
	    	// Return the ResultSet
	    	return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
    }

    /**
     * Imprime por pantalla el contenido de la tabla producto
     */
    public static void printTablaProducto() {
        ResultSet rs = getTablaProducto();
        // Loop the ResultSet
        try {
        	while (rs.next()) {
        		int id = rs.getInt("id");
        		String nombre = rs.getString("nombre");
        		int stock = rs.getInt("stock");
        		double precio = rs.getDouble("precio");
        		System.out.println("\nid: " + id + "\nnombre: " + nombre + "\nstock: " + stock+ "\nprecio: " + precio);
        	}
        } catch (SQLException e) {
        	e.printStackTrace();
        }
        
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE UN SOLO Producto
    //////////////////////////////////////////////////
    ;
    
    /**
     * Solicita a la BD el Producto con id indicado
     * @param id id del Producto
     * @return ResultSet con el resultado de la consulta, null en caso de error
     */
    public static ResultSet getProducto(int id) {
    	// Get the statement of the connection
    	PreparedStatement st;
		try {
	    	// Create the sql query
	    	String sql = "SELECT * FROM producto WHERE id = ?";
			st = conn.prepareStatement(sql);
			// Set the id parameter
			st.setInt(1, id);
	    	// Execute the query
	    	ResultSet rs = st.executeQuery();
	    	// Return the ResultSet
	    	return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
    }

    /**
     * Comprueba si en la BD existe el Producto con id indicado
     *
     * @param id id del Producto
     * @return verdadero si existe, false en caso contrario
     */
    public static boolean existsProducto(int id) {
        ResultSet rs = getProducto(id);
        if (rs == null) {
        	return false;
        } else {
        	return true;
        }
    }

    /**
     * Imprime los datos del Producto con id indicado
     *
     * @param id id del Producto
     */
    public static void printProducto(int id) {
    	if (existsProducto(id)) {
    		try {
    			ResultSet rs = getProducto(id);
            	while (rs.next()) {
            		int nid = rs.getInt("id");
            		String nombre = rs.getString("nombre");
            		int stock = rs.getInt("stock");
            		double precio = rs.getDouble("precio");
            		System.out.println("\nid: " + nid + "\nnombre: " + nombre + "\nstock: " + stock+ "\nprecio: " + precio);
            	}
            } catch (SQLException e) {
            	e.printStackTrace();
            }
    	} else {
    		System.out.println("El Producto con id = " + id + " no existe");
    	}
    }

    /**
     * Solicita a la BD insertar un nuevo registro Producto
     *
     * @param nombre nombre del Producto
     * @param stock stock del Producto
     * @param precio precio del Producto
     * @return verdadero si pudo insertarlo, false en caso contrario
     */
    public static boolean insertProducto(String nombre, int stock, double precio) {
    	// Get the statement of the connection
    	PreparedStatement st;
		try {
	    	// Create the sql query
	    	String sql = "INSERT INTO producto (nombre, stock, precio) VALUES (?, ?, ?)";
			st = conn.prepareStatement(sql);
			// Set the parameters
			st.setString(1, nombre);
			st.setInt(2, stock);
			st.setDouble(3, precio);
	    	// Execute the query
	    	st.execute();
	    	return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
    }

    /**
     * Solicita a la BD modificar los datos de un Producto
     *
     * @param id id del Producto a modificar
     * @param nombre nuevo nombre del Producto
     * @param stock nuevo stock del Producto
     * @param precio nuevo precio del Producto
     * @return verdadero si pudo modificarlo, false en caso contrario
     */
    public static boolean updateProducto(int id, String nombre, int stock, double precio) {
    	// Get the statement of the connection
    	PreparedStatement st;
		try {
	    	// Create the sql query
	    	String sql = "UPDATE producto SET nombre = ?, stock = ?, precio = ? WHERE id = ?";
			st = conn.prepareStatement(sql);
			// Set the parameters
			st.setString(1, nombre);
			st.setInt(2, stock);
			st.setDouble(3, precio);
			st.setInt(4, id);
	    	// Execute the query
			st.execute();
	    	return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
    }

    /**
     * Solicita a la BD eliminar un Producto
     *
     * @param id id del Producto a eliminar
     * @return verdadero si pudo eliminarlo, false en caso contrario
     */
    public static boolean deleteProducto(int id) {
    	// Get the statement of the connection
    	PreparedStatement st;
		try {
	    	// Create the sql query
	    	String sql = "DELETE FROM producto WHERE id = ?";
			st = conn.prepareStatement(sql);
			// Set the id parameter
			st.setInt(1, id);
	    	// Execute the query
	    	st.execute();
	    	return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
    }

}
