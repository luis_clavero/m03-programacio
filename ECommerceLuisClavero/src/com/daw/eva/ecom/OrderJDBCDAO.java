package com.daw.eva.ecom;


import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

/**
 *
 * @author eva
 * TODO
 * Cal definir els contractes de la interfície ClientDAO i demés entitats, i implementar-les 
 * Cal crear de la mateixa manera, ProducteJDBCDAO que implementa ProducteDAO, i idem per a ComandaJDBCDAO
 */
public class OrderJDBCDAO implements ClientDAO {

    // Conexión a la base de datos
    private static Connection conn = null;

    // Configuración de la conexión a la base de datos
    private static final String DB_HOST = "localhost";
    private static final String DB_PORT = "3306";
    private static final String DB_NAME = "tienda";
    private static final String DB_URL = "jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "?serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "Kaincc2127";
    private static final String DB_MSQ_CONN_OK = "CONEXIÓN CORRECTA";
    private static final String DB_MSQ_CONN_NO = "ERROR EN LA CONEXIÓN";

    //////////////////////////////////////////////////
    // MÉTODOS DE CONEXIÓN A LA BASE DE DATOS
    //////////////////////////////////////////////////
    
    /**
     * Intenta cargar el JDBC driver.
     * @return true si pudo cargar el driver, false en caso contrario
     */
    public static boolean loadDriver() {
        try {
            System.out.print("Loading Driver...");
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            System.out.println("OK!");
            return true;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Intenta conectar con la base de datos.
     *
     * @return true si pudo conectarse, false en caso contrario
     */
    public static boolean connect() {
    	// Establish the connection
        try {
        	conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	System.out.println(DB_MSQ_CONN_OK);
        	return true;
        } catch (Exception e) {
        	System.out.println(DB_MSQ_CONN_NO);
        	System.out.println(e);
        	return false;
        }
    }

    /**
     * Comprueba la conexión y muestra su estado por pantalla
     *
     * @return true si la conexión existe y es válida, false en caso contrario
     */
    public static boolean isConnected() {
        // Check if the connection is valid and it's not closed
        try {
        	if (conn.isValid(10) && !conn.isClosed()) {
        		return true;
        	}
        } catch (SQLException e) {
        	e.printStackTrace();
        }
        return false;
    }

    /**
     * Cierra la conexión con la base de datos
     */
    public static void close() {
        try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE TABLA pedido
    //////////////////////////////////////////////////
    
    // Devuelve 
    // Los argumentos indican el tipo de ResultSet deseado
    /**
     * Obtiene toda la tabla pedido de la base de datos
     * @param resultSetType Tipo de ResultSet
     * @param resultSetConcurrency Concurrencia del ResultSet
     * @return ResultSet (del tipo indicado) con la tabla, null en caso de error
     */
    public static ResultSet getTablaPedido(int resultSetType, int resultSetConcurrency) {
    	// Get the statement of the connection
    	Statement st;
		try {
			st = conn.createStatement(resultSetType, resultSetConcurrency);
	    	// Create the sql query
	    	String sql = "SELECT * FROM pedido";
	    	// Execute the query
	    	ResultSet rs = st.executeQuery(sql);
	    	// Return the ResultSet
	    	return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
    }

    /**
     * Obtiene toda la tabla pedido de la base de datos
     *
     * @return ResultSet (por defecto) con la tabla, null en caso de error
     */
    public static ResultSet getTablaPedido() {
    	// Get the statement of the connection
    	Statement st;
		try {
			st = conn.createStatement();
	    	// Create the sql query
	    	String sql = "SELECT * FROM pedido";
	    	// Execute the query
	    	ResultSet rs = st.executeQuery(sql);
	    	// Return the ResultSet
	    	return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
    }

    /**
     * Imprime por pantalla el contenido de la tabla pedido
     */
    public static void printTablaPedido() {
        ResultSet rs = getTablaPedido();
        // Loop the ResultSet
        try {
        	while (rs.next()) {
        		int id = rs.getInt("id");
        		int idProd = rs.getInt("idProd");
        		int idCli = rs.getInt("idCli");
        		int cantidad = rs.getInt("cantidad");
        		double importe = rs.getDouble("importe");
        		System.out.println("\nid: " + id + "\nidProd: " + idProd + "\nidCli: " + idCli+ "\ncantidad: " + cantidad + "\nimporte: " + importe);
        	}
        } catch (SQLException e) {
        	e.printStackTrace();
        }
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE UN SOLO Pedido
    //////////////////////////////////////////////////
    
    /**
     * Solicita a la BD el Pedido con id indicado
     * @param id id del Pedido
     * @return ResultSet con el resultado de la consulta, null en caso de error
     */
    public static ResultSet getPedido(int id) {
    	// Get the statement of the connection
    	PreparedStatement st;
		try {
	    	// Create the sql query
	    	String sql = "SELECT * FROM pedido WHERE id = ?";
			st = conn.prepareStatement(sql);
			// Set the id parameter
			st.setInt(1, id);
	    	// Execute the query
	    	ResultSet rs = st.executeQuery();
	    	// Return the ResultSet
	    	return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
    }

    /**
     * Comprueba si en la BD existe el Pedido con id indicado
     *
     * @param id id del Pedido
     * @return verdadero si existe, false en caso contrario
     */
    public static boolean existsPedido(int id) {
        ResultSet rs = getPedido(id);
        if (rs == null) {
        	return false;
        } else {
        	return true;
        }
    }

    /**
     * Imprime los datos del Pedido con id indicado
     *
     * @param id id del Pedido
     */
    public static void printPedido(int id) {
    	if (existsPedido(id)) {
    		try {
    			ResultSet rs = getPedido(id);
            	while (rs.next()) {
            		int nid = rs.getInt("id");
            		int idProd = rs.getInt("idProd");
            		int idCli = rs.getInt("idCli");
            		int cantidad = rs.getInt("cantidad");
            		double importe = rs.getDouble("importe");
            		System.out.println("\nid: " + nid + "\nidProd: " + idProd + "\nidCli: " + idCli+ "\ncantidad: " + cantidad + "\nimporte: " + importe);
            	}
            } catch (SQLException e) {
            	e.printStackTrace();
            }
    	} else {
    		System.out.println("El Pedido con id = " + id + " no existe");
    	}
    }

    /**
     * Solicita a la BD insertar un nuevo registro Pedido
     *
     * @param idProd idProd del Pedido
     * @param idCli idCli del Pedido
     * @param cantidad cantidad del producto en el pedido
     * @param importe importe del pedido
     * @return verdadero si pudo insertarlo, false en caso contrario
     */
    public static boolean insertPedido(int idProd, int idCli, int cantidad, double importe) {
    	// Get the statement of the connection
    	PreparedStatement st;
		try {
	    	// Create the sql query
	    	String sql = "INSERT INTO pedido (idProd, idCli, cantidad, importe) VALUES (?, ?, ?, ?)";
			st = conn.prepareStatement(sql);
			// Set the parameters
			st.setInt(1, idProd);
			st.setInt(2, idCli);
			st.setInt(3, cantidad);
			st.setDouble(4, importe);
	    	// Execute the query
	    	st.execute();
	    	return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
    }

    /**
     * Solicita a la BD modificar los datos de un Pedido
     *
     * @param id id del Pedido a modificar
     * @param idProd idProd del Pedido a modificar
     * @param idCli idCli del Pedido a modificar
     * @param cantidad cantidad del producto en el pedido a modificar
     * @param importe importe del pedido a modificar
     * @return verdadero si pudo modificarlo, false en caso contrario
     */
    public static boolean updatePedido(int id, int idProd, int idCli, int cantidad, double importe) {
    	// Get the statement of the connection
    	PreparedStatement st;
		try {
	    	// Create the sql query
	    	String sql = "UPDATE pedido SET idProd = ?, idCli = ?, cantidad = ?, importe = ? WHERE id = ?";
			st = conn.prepareStatement(sql);
			// Set the parameters
			st.setInt(1, idProd);
			st.setInt(2, idCli);
			st.setInt(3, cantidad);
			st.setDouble(4, importe);
			st.setInt(5, id);
	    	// Execute the query
			st.execute();
	    	return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
    }

    /**
     * Solicita a la BD eliminar un Pedido
     *
     * @param id id del Pedido a eliminar
     * @return verdadero si pudo eliminarlo, false en caso contrario
     */
    public static boolean deletePedido(int id) {
    	// Get the statement of the connection
    	PreparedStatement st;
		try {
	    	// Create the sql query
	    	String sql = "DELETE FROM pedido WHERE id = ?";
			st = conn.prepareStatement(sql);
			// Set the id parameter
			st.setInt(1, id);
	    	// Execute the query
	    	st.execute();
	    	return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
    }

}
