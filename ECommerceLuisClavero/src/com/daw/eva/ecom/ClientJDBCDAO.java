package com.daw.eva.ecom;


import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

/**
 *
 * @author eva
 * TODO
 * Cal definir els contractes de la interfície ClientDAO i demés entitats, i implementar-les 
 * Cal crear de la mateixa manera, ProducteJDBCDAO que implementa ProducteDAO, i idem per a ComandaJDBCDAO
 */
public class ClientJDBCDAO implements ClientDAO {

    // Conexión a la base de datos
    private static Connection conn = null;

    // Configuración de la conexión a la base de datos
    private static final String DB_HOST = "localhost";
    private static final String DB_PORT = "3306";
    private static final String DB_NAME = "tienda";
    private static final String DB_URL = "jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "?serverTimezone=UTC";
    private static final String DB_USER = "root";
    private static final String DB_PASS = "Kaincc2127";
    private static final String DB_MSQ_CONN_OK = "CONEXIÓN CORRECTA";
    private static final String DB_MSQ_CONN_NO = "ERROR EN LA CONEXIÓN";

    // Configuración de la tabla Clientes
    private static final String DB_CLI = "clientes";
    private static final String DB_CLI_SELECT = "SELECT * FROM " + DB_CLI;
    private static final String DB_CLI_ID = "id";
    private static final String DB_CLI_NOM = "nombre";
    private static final String DB_CLI_DIR = "direccion";

    //////////////////////////////////////////////////
    // MÉTODOS DE CONEXIÓN A LA BASE DE DATOS
    //////////////////////////////////////////////////
    ;
    
    /**
     * Intenta cargar el JDBC driver.
     * @return true si pudo cargar el driver, false en caso contrario
     */
    public static boolean loadDriver() {
        try {
            System.out.print("Loading Driver...");
            Class.forName("com.mysql.cj.jdbc.Driver").newInstance();
            System.out.println("OK!");
            return true;
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }

    /**
     * Intenta conectar con la base de datos.
     *
     * @return true si pudo conectarse, false en caso contrario
     */
    public static boolean connect() {
    	// Establish the connection
        try {
        	conn = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
        	System.out.println(DB_MSQ_CONN_OK);
        	return true;
        } catch (Exception e) {
        	System.out.println(DB_MSQ_CONN_NO);
        	System.out.println(e);
        	return false;
        }
    }

    /**
     * Comprueba la conexión y muestra su estado por pantalla
     *
     * @return true si la conexión existe y es válida, false en caso contrario
     */
    public static boolean isConnected() {
        // Check if the connection is valid and it's not closed
        try {
        	if (conn.isValid(10) && !conn.isClosed()) {
        		return true;
        	}
        } catch (SQLException e) {
        	e.printStackTrace();
        }
        return false;
    }

    /**
     * Cierra la conexión con la base de datos
     */
    public static void close() {
        try {
			conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE TABLA CLIENTES
    //////////////////////////////////////////////////
    ;
    
    // Devuelve 
    // Los argumentos indican el tipo de ResultSet deseado
    /**
     * Obtiene toda la tabla clientes de la base de datos
     * @param resultSetType Tipo de ResultSet
     * @param resultSetConcurrency Concurrencia del ResultSet
     * @return ResultSet (del tipo indicado) con la tabla, null en caso de error
     */
    public static ResultSet getTablaClientes(int resultSetType, int resultSetConcurrency) {
    	// Get the statement of the connection
    	Statement st;
		try {
			st = conn.createStatement(resultSetType, resultSetConcurrency);
	    	// Create the sql query
	    	String sql = "SELECT * FROM clientes";
	    	// Execute the query
	    	ResultSet rs = st.executeQuery(sql);
	    	// Return the ResultSet
	    	return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
    }

    /**
     * Obtiene toda la tabla clientes de la base de datos
     *
     * @return ResultSet (por defecto) con la tabla, null en caso de error
     */
    public static ResultSet getTablaClientes() {
    	// Get the statement of the connection
    	Statement st;
		try {
			st = conn.createStatement();
	    	// Create the sql query
	    	String sql = "SELECT * FROM clientes";
	    	// Execute the query
	    	ResultSet rs = st.executeQuery(sql);
	    	// Return the ResultSet
	    	return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
    }

    /**
     * Imprime por pantalla el contenido de la tabla clientes
     */
    public static void printTablaClientes() {
        ResultSet rs = getTablaClientes();
        // Loop the ResultSet
        try {
        	while (rs.next()) {
        		int id = rs.getInt(DB_CLI_ID);
        		String nombre = rs.getString(DB_CLI_NOM);
        		String direccion = rs.getString(DB_CLI_DIR);
        		System.out.println("\nid: " + id + "\nnombre: " + nombre + "\ndireccion: " + direccion);
        	}
        } catch (SQLException e) {
        	e.printStackTrace();
        }
        
    }

    //////////////////////////////////////////////////
    // MÉTODOS DE UN SOLO CLIENTE
    //////////////////////////////////////////////////
    ;
    
    /**
     * Solicita a la BD el cliente con id indicado
     * @param id id del cliente
     * @return ResultSet con el resultado de la consulta, null en caso de error
     */
    public static ResultSet getCliente(int id) {
    	// Get the statement of the connection
    	PreparedStatement st;
		try {
	    	// Create the sql query
	    	String sql = "SELECT * FROM clientes WHERE id = ?";
			st = conn.prepareStatement(sql);
			// Set the id parameter
			st.setInt(1, id);
	    	// Execute the query
	    	ResultSet rs = st.executeQuery();
	    	// Return the ResultSet
	    	return rs;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
    }

    /**
     * Comprueba si en la BD existe el cliente con id indicado
     *
     * @param id id del cliente
     * @return verdadero si existe, false en caso contrario
     */
    public static boolean existsCliente(int id) {
        ResultSet rs = getCliente(id);
        if (rs == null) {
        	return false;
        } else {
        	return true;
        }
    }

    /**
     * Imprime los datos del cliente con id indicado  --> Carga objecto Client
     *
     * @param id id del cliente
     */
    public static void printCliente(int id) {
    	if (existsCliente(id)) {
    		try {
    			ResultSet rs = getCliente(id);
            	while (rs.next()) {
            		int nid = rs.getInt(DB_CLI_ID);
            		String nombre = rs.getString(DB_CLI_NOM);
            		String direccion = rs.getString(DB_CLI_DIR);
            		System.out.println("\nid: " + nid + "\nnombre: " + nombre + "\ndireccion: " + direccion + "\n");
            	}
            } catch (SQLException e) {
            	e.printStackTrace();
            }
    	} else {
    		System.out.println("El cliente con id = " + id + " no existe");
    	}
    }

    /**
     * Solicita a la BD insertar un nuevo registro cliente
     *
     * @param nombre nombre del cliente
     * @param direccion dirección del cliente
     * @return verdadero si pudo insertarlo, false en caso contrario
     */
    public static boolean insertCliente(String nombre, String direccion) {
    	// Get the statement of the connection
    	PreparedStatement st;
		try {
	    	// Create the sql query
	    	String sql = "INSERT INTO clientes (nombre, direccion) VALUES (?, ?)";
			st = conn.prepareStatement(sql);
			// Set the parameters
			st.setString(1, nombre);
			st.setString(2, direccion);
	    	// Execute the query
	    	st.execute();
	    	return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
    }

    /**
     * Solicita a la BD modificar los datos de un cliente
     *
     * @param id id del cliente a modificar
     * @param nombre nuevo nombre del cliente
     * @param direccion nueva dirección del cliente
     * @return verdadero si pudo modificarlo, false en caso contrario
     */
    public static boolean updateCliente(int id, String nuevoNombre, String nuevaDireccion) {
    	// Get the statement of the connection
    	PreparedStatement st;
		try {
	    	// Create the sql query
	    	String sql = "UPDATE clientes SET nombre = ?, direccion = ? WHERE id = ?";
			st = conn.prepareStatement(sql);
			// Set the parameters
			st.setString(1, nuevoNombre);
			st.setString(2, nuevaDireccion);
			st.setInt(3, id);
	    	// Execute the query
			st.execute();
	    	return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
    }

    /**
     * Solicita a la BD eliminar un cliente
     *
     * @param id id del cliente a eliminar
     * @return verdadero si pudo eliminarlo, false en caso contrario
     */
    public static boolean deleteCliente(int id) {
    	// Get the statement of the connection
    	PreparedStatement st;
		try {
	    	// Create the sql query
	    	String sql = "DELETE FROM clientes WHERE id = ?";
			st = conn.prepareStatement(sql);
			// Set the id parameter
			st.setInt(1, id);
	    	// Execute the query
	    	st.execute();
	    	return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
    }

}
