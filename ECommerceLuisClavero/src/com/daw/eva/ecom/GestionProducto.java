package com.daw.eva.ecom;


import java.util.Scanner;

/**
 *
 * @author eva
 */
public class GestionProducto {

    public static void main(String[] args) {

    	//TODO Controlar excepcions aquí al main ...
    	
        ProductJDBCDAO.loadDriver();
        ProductJDBCDAO.connect();

        boolean salir = false;
        do {
            salir = menuPrincipal();
        } while (!salir);

        ProductJDBCDAO.close();

    }

    public static boolean menuPrincipal() {
        System.out.println("");
        System.out.println("MENU PRINCIPAL");
        System.out.println("1. Listar productos");
        System.out.println("2. Nuevo producto");
        System.out.println("3. Modificar producto");
        System.out.println("4. Eliminar producto");
        System.out.println("5. Salir");
        
      //TODO ampliem menu amb Productes i comandes.....
        // Veure comandes ha de mostrar idProducte, idComanda i noms, quantitat
        
        Scanner in = new Scanner(System.in);
            
        int opcion = pideInt("Elige una opción: ");
        
        switch (opcion) {
            case 1:
                opcionMostrarProductos();
                return false;
            case 2:
                opcionNuevoProducto();
                return false;
            case 3:
                opcionModificarProducto();
                return false;
            case 4:
                opcionEliminarProducto();
                return false;
            case 5:
                return true;
            default:
                System.out.println("Opción elegida incorrecta");
                return false;
        }
        
    }
    
    public static int pideInt(String mensaje){
        
        while(true) {
            try {
                System.out.print(mensaje);
                Scanner in = new Scanner(System.in);
                int valor = in.nextInt();
                //in.nextLine();
                return valor;
            } catch (Exception e) {
                System.out.println("No has introducido un número entero. Vuelve a intentarlo.");
            }
        }
    }
    
    public static String pideLinea(String mensaje){
        
        while(true) {
            try {
                System.out.print(mensaje);
                Scanner in = new Scanner(System.in);
                String linea = in.nextLine();
                return linea;
            } catch (Exception e) {
                System.out.println("No has introducido una cadena de texto. Vuelve a intentarlo.");
            }
        }
    }
    
    public static double pideDouble(String mensaje){
        
        while(true) {
            try {
                System.out.print(mensaje);
                Scanner in = new Scanner(System.in);
                double doble = in.nextDouble();
                return doble;
            } catch (Exception e) {
                System.out.println("No has introducido un flotante. Vuelve a intentarlo.");
            }
        }
    }

    public static void opcionMostrarProductos() {
        System.out.println("Listado de Productos:");
        ProductJDBCDAO.printTablaProducto();
    }

    public static void opcionNuevoProducto() {
        Scanner in = new Scanner(System.in);

        System.out.println("Introduce los datos del nuevo producto:");
        String nombre = pideLinea("Nombre: ");
        int stock = pideInt("Stock: ");
        double precio = pideDouble("Precio: ");

        boolean res = ProductJDBCDAO.insertProducto(nombre, stock, precio);

        if (res) {
            System.out.println("Producto registrado correctamente");
        } else {
            System.out.println("Error :(");
        }
    }

    public static void opcionModificarProducto() {
        Scanner in = new Scanner(System.in);

        int id = pideInt("Indica el id del producto a modificar: ");

        // Comprobamos si existe el producto
        if (!ProductJDBCDAO.existsProducto(id)) {
            System.out.println("El producto " + id + " no existe.");
            return;
        }

        // Mostramos datos del producto a modificar
        ProductJDBCDAO.printProducto(id);

        // Solicitamos los nuevos datos
        String nombre = pideLinea("Nuevo nombre: ");
        int stock = pideInt("Nuevo stock: ");
        double precio = pideDouble("Nuevo precio: ");

        // Registramos los cambios
        boolean res = ProductJDBCDAO.updateProducto(id, nombre, stock, precio);

        if (res) {
            System.out.println("Producto modificado correctamente");
        } else {
            System.out.println("Error :(");
        }
    }

    public static void opcionEliminarProducto() {
        Scanner in = new Scanner(System.in);

        int id = pideInt("Indica el id del producto a eliminar: ");

        // Comprobamos si existe el producto
        if (!ProductJDBCDAO.existsProducto(id)) {
            System.out.println("El producto " + id + " no existe.");
            return;
        }

        // Eliminamos el producto
        boolean res = ProductJDBCDAO.deleteProducto(id);

        if (res) {
            System.out.println("Producto eliminado correctamente");
        } else {
            System.out.println("Error :(");
        }
    }
}
