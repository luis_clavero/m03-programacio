package com.daw.eva.ecom;


import java.util.Scanner;

/**
 *
 * @author eva
 */
public class GestionPedido {

    public static void main(String[] args) {

    	//TODO Controlar excepcions aquí al main ...
    	
        OrderJDBCDAO.loadDriver();
        OrderJDBCDAO.connect();

        boolean salir = false;
        do {
            salir = menuPrincipal();
        } while (!salir);

        OrderJDBCDAO.close();

    }

    public static boolean menuPrincipal() {
        System.out.println("");
        System.out.println("MENU PRINCIPAL");
        System.out.println("1. Listar pedidos");
        System.out.println("2. Nuevo pedido");
        System.out.println("3. Modificar pedido");
        System.out.println("4. Eliminar pedido");
        System.out.println("5. Salir");
        
      //TODO ampliem menu amb Orderes i comandes.....
        // Veure comandes ha de mostrar idOrdere, idComanda i noms, quantitat
        
        Scanner in = new Scanner(System.in);
            
        int opcion = pideInt("Elige una opción: ");
        
        switch (opcion) {
            case 1:
                opcionMostrarPedidos();
                return false;
            case 2:
                opcionNuevoPedido();
                return false;
            case 3:
                opcionModificarPedido();
                return false;
            case 4:
                opcionEliminarPedido();
                return false;
            case 5:
                return true;
            default:
                System.out.println("Opción elegida incorrecta");
                return false;
        }
        
    }
    
    public static int pideInt(String mensaje){
        
        while(true) {
            try {
                System.out.print(mensaje);
                Scanner in = new Scanner(System.in);
                int valor = in.nextInt();
                //in.nextLine();
                return valor;
            } catch (Exception e) {
                System.out.println("No has introducido un número entero. Vuelve a intentarlo.");
            }
        }
    }
    
    public static String pideLinea(String mensaje){
        
        while(true) {
            try {
                System.out.print(mensaje);
                Scanner in = new Scanner(System.in);
                String linea = in.nextLine();
                return linea;
            } catch (Exception e) {
                System.out.println("No has introducido una cadena de texto. Vuelve a intentarlo.");
            }
        }
    }
    
    public static double pideDouble(String mensaje){
        
        while(true) {
            try {
                System.out.print(mensaje);
                Scanner in = new Scanner(System.in);
                double doble = in.nextDouble();
                return doble;
            } catch (Exception e) {
                System.out.println("No has introducido un flotante. Vuelve a intentarlo.");
            }
        }
    }

    public static void opcionMostrarPedidos() {
        System.out.println("Listado de Pedidos:");
        OrderJDBCDAO.printTablaPedido();
    }

    public static void opcionNuevoPedido() {
        Scanner in = new Scanner(System.in);

        System.out.println("Introduce los datos del nuevo pedido:");
        int idProd = pideInt("idProd: ");
        int idCli = pideInt("idCli: ");
        int cantidad = pideInt("Cantidad: ");
        double importe = pideDouble("Importe: ");

        boolean res = OrderJDBCDAO.insertPedido(idProd, idCli, cantidad, importe);

        if (res) {
            System.out.println("Pedido registrado correctamente");
        } else {
            System.out.println("Error :(");
        }
    }

    public static void opcionModificarPedido() {
        Scanner in = new Scanner(System.in);

        int id = pideInt("Indica el id del pedido a modificar: ");

        // Comprobamos si existe el pedido
        if (!OrderJDBCDAO.existsPedido(id)) {
            System.out.println("El pedido " + id + " no existe.");
            return;
        }

        // Mostramos datos del pedido a modificar
        OrderJDBCDAO.printPedido(id);

        // Solicitamos los nuevos datos
        int idProd = pideInt("idProd nuevo: ");
        int idCli = pideInt("idCli nuevo: ");
        int cantidad = pideInt("Cantidad nuevo: ");
        double importe = pideDouble("Importe nuevo: ");

        // Registramos los cambios
        boolean res = OrderJDBCDAO.updatePedido(id, idProd, idCli, cantidad, importe);

        if (res) {
            System.out.println("Pedido modificado correctamente");
        } else {
            System.out.println("Error :(");
        }
    }

    public static void opcionEliminarPedido() {
        Scanner in = new Scanner(System.in);

        int id = pideInt("Indica el id del pedido a eliminar: ");

        // Comprobamos si existe el pedido
        if (!OrderJDBCDAO.existsPedido(id)) {
            System.out.println("El pedido " + id + " no existe.");
            return;
        }

        // Eliminamos el pedido
        boolean res = OrderJDBCDAO.deletePedido(id);

        if (res) {
            System.out.println("Pedido eliminado correctamente");
        } else {
            System.out.println("Error :(");
        }
    }
}
