package com.daw.eva.ecom;

import java.sql.ResultSet;
import java.util.List;

import com.daw.eva.ecom.DAOException;
import com.daw.eva.ecom.business.entities.Client;

public interface ClientDAO {

	//TO-DO crea el contracte o llista de mètodes a oferir
	
	public ResultSet getTablaClientes();
    public ResultSet getCliente(int id);
    public boolean insertCliente(String nombre, String direccion);
    public boolean updateCliente(String nombre, String direccion);
    public boolean deleteCliente(int id);
}
