package Llibreries;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public abstract class Data {

	public static final DateTimeFormatter PATTERN = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");

	public static String imprimirData(LocalDateTime dataTmp) {
		String formatedDate;
		// Check if the date is null
		if (dataTmp == null) {
			formatedDate = "NULL";
			return formatedDate;
		} else {
			formatedDate = dataTmp.format(Data.PATTERN);
			return formatedDate;
		}
	}
	
	public static boolean esData(String dataTmp) {
		// Split the data so i can iterate each piece and check if it's a number
		String[] splittedData = dataTmp.split("-");
		for (String str : splittedData) {
			// Check if the item is a number, if it's not return false
			if (!Cadena.stringIsInt(str)) {
				return false;
			}
		}
		return true;
	}
	
}