package com.daw.ClaveroLuis.krona;

import java.time.LocalDateTime;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Deque;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.TimeUnit;

import Llibreries.Cadena;
import Llibreries.Data;

public class Ruta {

	public static List<Waypoint_Dades> crearRutaInicial() {
		List<Waypoint_Dades> llistaWaypointLinkedList = null;

		llistaWaypointLinkedList = new LinkedList<Waypoint_Dades>();

		llistaWaypointLinkedList.add(new Waypoint_Dades(0, "Òrbita de la Terra", new int[] { 0, 0, 0 }, true,
				LocalDateTime.parse("21-10-2020 01:10", Data.PATTERN), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(1, "Punt Lagrange Terra-LLuna", new int[] { 1, 1, 1 }, true,
				LocalDateTime.parse("21-10-2020 01:00", Data.PATTERN), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(2, "Òrbita de la LLuna", new int[] { 2, 2, 2 }, true,
				LocalDateTime.parse("21-10-2020 00:50", Data.PATTERN), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(3, "Òrbita de Mart", new int[] { 3, 3, 3 }, true,
				LocalDateTime.parse("21-10-2020 00:40", Data.PATTERN), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(4, "Òrbita de Júpiter", new int[] { 4, 4, 4 }, true,
				LocalDateTime.parse("21-10-2020 00:30", Data.PATTERN), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(5, "Punt Lagrange Júpiter-Europa", new int[] { 5, 5, 5 }, true,
				LocalDateTime.parse("21-10-2020 00:20", Data.PATTERN), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(6, "Òrbita de Europa", new int[] { 6, 6, 6 }, true,
				LocalDateTime.parse("21-10-2020 00:10", Data.PATTERN), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN)));
		llistaWaypointLinkedList.add(new Waypoint_Dades(7, "Òrbita de Venus", new int[] { 7, 7, 7 }, true,
				LocalDateTime.parse("21-10-2020 00:01", Data.PATTERN), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN)));

		return llistaWaypointLinkedList;
	}

	public static ComprovacioRendiment inicialitzarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		Deque<Waypoint_Dades> tmpQueue = new ArrayDeque<Waypoint_Dades>(Ruta.crearRutaInicial());
		// Add the waypoints to the object queue
		for (int i = 0; i < tmpQueue.size(); i++) {
			// Remove the last one and add it to the other queue
			comprovacioRendimentTmp.pilaWaypoints.offerLast(tmpQueue.pop());
		}
		Waypoint_Dades wp = new Waypoint_Dades(4, "Òrbita de Júpiter REPETIDA", new int[] { 4, 4, 4 }, true,
				LocalDateTime.parse("21-10-2020 00:30", Data.PATTERN), null,
				LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN));
		// Add the new wp to the queue
		comprovacioRendimentTmp.pilaWaypoints.offerLast(wp);
		// Add the wp to the attribute
		comprovacioRendimentTmp.wtmp = wp;
		return comprovacioRendimentTmp;
	}

	public static void visualitzarRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		// Print the info of each waypoint
		for (Waypoint_Dades wp : comprovacioRendimentTmp.pilaWaypoints) {
			System.out.println(wp);
		}
	}

	public static void invertirRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		Deque<Waypoint_Dades> pilaWaypointsInversa = new ArrayDeque<Waypoint_Dades>();
		// Reverse the order of the queue
		for (int i = 0; i < comprovacioRendimentTmp.pilaWaypoints.size(); i++) {
			pilaWaypointsInversa.offerFirst(comprovacioRendimentTmp.pilaWaypoints.pop());
		}
		// Now print the info of the queue reversed
		for (Waypoint_Dades wp : pilaWaypointsInversa) {
			System.out.println(wp);
		}
	}

	public static void existeixWaypointEnRuta(ComprovacioRendiment comprovacioRendimentTmp) {
		boolean isInQueue = false;
		Waypoint_Dades wp;
		// Get each one of the waypoints and check if it's equal to the wtmp one
		while (!isInQueue) {
			wp = comprovacioRendimentTmp.pilaWaypoints.pop();
			if (wp.equals(comprovacioRendimentTmp.wtmp)) {
				isInQueue = true;
			}
		}
		if (isInQueue) {
			System.out.println("El waypoint wtmp està a la cua.");
		} else {
			System.out.println("El waypoint wtmp no està a la cua.");
		}
		wp = comprovacioRendimentTmp.wtmp;
		isInQueue = false;
		// Do the same with the new wtmp waypoint
		while (!isInQueue) {
			wp = comprovacioRendimentTmp.pilaWaypoints.pop();
			if (wp.equals(comprovacioRendimentTmp.wtmp)) {
				isInQueue = true;
			}
		}
		if (isInQueue) {
			System.out.println("El waypoint wtmp està a la cua.");
		} else {
			System.out.println("El waypoint wtmp no està a la cua.");
		}
	}

	public static ComprovacioRendiment inicialitzarRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		// Create 5 new routes
		Ruta_Dades ruta_0 = new Ruta_Dades(0, "ruta   0:   Terra   -->   Punt   Lagrange   Júpiter-Europa",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5)), true,
				LocalDateTime.parse("28-10-2020 16:30", Data.PATTERN), null,
				LocalDateTime.parse("28-10-2020 16:30", Data.PATTERN));
		Ruta_Dades ruta_1 = new Ruta_Dades(1, "ruta 1: Terra --> Òrbita de Mart (directe)",
				new ArrayList<Integer>(Arrays.asList(0, 3)), true,
				LocalDateTime.parse("28-10-2020 16:31", Data.PATTERN), null,
				LocalDateTime.parse("28-10-2020 16:31", Data.PATTERN));
		Ruta_Dades ruta_2 = new Ruta_Dades(2, "ruta 2.1: Terra --> Òrbita de Venus",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true,
				LocalDateTime.parse("28-10-2020 16:32", Data.PATTERN), null,
				LocalDateTime.parse("28-10-2020 16:32", Data.PATTERN));
		Ruta_Dades ruta_3 = new Ruta_Dades(3, "ruta 3: Terra --> Mart (directe) --> Òrbita de Júpiter ",
				new ArrayList<Integer>(Arrays.asList(0, 3, 4)), true,
				LocalDateTime.parse("28-10-2020 16:33", Data.PATTERN), null,
				LocalDateTime.parse("28-10-2020 16:33", Data.PATTERN));
		Ruta_Dades ruta_4 = new Ruta_Dades(4, "ruta   2.2:   Terra   -->   Òrbita   de   Venus   (REPETIDA)",
				new ArrayList<Integer>(Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7)), true,
				LocalDateTime.parse("28-10-2020 16:32", Data.PATTERN), null,
				LocalDateTime.parse("30-10-2020 19:49", Data.PATTERN));
		// Add them to the ArrayList
		comprovacioRendimentTmp.llistaRutes.add(ruta_0);
		comprovacioRendimentTmp.llistaRutes.add(ruta_1);
		comprovacioRendimentTmp.llistaRutes.add(ruta_2);
		comprovacioRendimentTmp.llistaRutes.add(ruta_3);
		comprovacioRendimentTmp.llistaRutes.add(ruta_4);
		// Iterate all of them and print their info
		for (Ruta_Dades route : comprovacioRendimentTmp.llistaRutes) {
			System.out.println(route.getNom() + ": waypoints" + route.getWaypoints());
		}
		return comprovacioRendimentTmp;
	}

	public static void setUnio(ComprovacioRendiment comprovacioRendimentTmp) {
		// Create the new HashSet
		HashSet<Integer> hashTmp = new HashSet<Integer>();
		// Iterate each item in the list to add the waypoints into the hash
		for (Ruta_Dades route : comprovacioRendimentTmp.llistaRutes) {
			hashTmp.addAll(route.getWaypoints());
		}
		// Print the whole hash
		System.out.println("ID dels waypoints ficats en el set: " + hashTmp);
	}

	public static void setInterseccio(ComprovacioRendiment comprovacioRendimentTmp) {
		HashSet<Integer> intersection = new HashSet<Integer>();
		boolean isInEachList = true;
		// Get the first waypoints in the ArrayList
		ArrayList<Integer> firstWaypoints = comprovacioRendimentTmp.llistaRutes.get(0).getWaypoints();
		// Get the length of the first waypoint list on the ArrayList
		int waypointLength = firstWaypoints.size();
		// Iterate each route and check if any of thoose contains the number
		for (int i = 0; i < waypointLength; i++) {
			for (Ruta_Dades route : comprovacioRendimentTmp.llistaRutes) {
				// Check if the number is not in the list
				if (!route.getWaypoints().contains(firstWaypoints.get(i))) {
					isInEachList = false;
				}
			}
			// If the number was not in the list means it's not an intersection
			if (isInEachList) {
				// Check the hash does not contain the number already
				if (!intersection.contains(firstWaypoints.get(i))) {
					// If passes those conditions add it to the hash
					intersection.add(firstWaypoints.get(i));
				}
			}
			isInEachList = true;
		}
		// Print the info of the hash
		System.out.println("ID dels waypoints en totes les rutes: " + intersection);
	}

	private static int buscarRuta(int numRuta, ComprovacioRendiment comprovacioRendimentTmp) {
		// Iterate the ArrayList
		for (Ruta_Dades route : comprovacioRendimentTmp.llistaRutes) {
			// Check if the list contains a route with this id
			if (route.getId() == numRuta) {
				return comprovacioRendimentTmp.llistaRutes.indexOf(route);
			}
		}
		return -1;
	}

	public static void setResta(ComprovacioRendiment comprovacioRendimentTmp) {
		int currentNumber, indexOfRoute;
		Scanner s = new Scanner(System.in);
		String waypointsIntroduced;
		String[] numbers;
		boolean correctWaypoints = false;
		HashSet<Integer> hashTmp = new HashSet<Integer>();
		// Iterate all of them and print their info
		for (Ruta_Dades route : comprovacioRendimentTmp.llistaRutes) {
			System.out.println("ID " + route.getId() + ": " + route.getNom() + ": waypoints" + route.getWaypoints());
		}
		// Ask for the waypoints while they're not correct
		while (!correctWaypoints) {
			System.out.print("Selecciona ruta A i B (format: 3 17): ");
			waypointsIntroduced = s.nextLine();
			// Split the string to get the IDs
			numbers = waypointsIntroduced.split(" ");
			// If the length of the array is not 2 means another numbers have to be asked
			if (numbers.length == 2) {
				// Iterate the ID's in the array
				for (String str : numbers) {
					// Try to parse the string to an int and see if it can be possible
					try {
						currentNumber = Integer.parseInt(str);
					} catch (NumberFormatException nfe) {
						System.out.println(
								"ERROR: has introduït " + str + " com a ruta. Els ID de les rutes són enters.");
						break;
					}
					// Check if the number exists in the route list
					indexOfRoute = Ruta.buscarRuta(currentNumber, comprovacioRendimentTmp);
					if (indexOfRoute != -1) {
						// If it's the first number add the waypoints to the list, remove them otherwise
						if (str.equals(numbers[0])) {
							hashTmp.addAll(comprovacioRendimentTmp.llistaRutes.get(indexOfRoute).getWaypoints());
							System.out.println();
							System.out.println("HashSet (havent-hi afegit el waypoints de la ruta A) = " + hashTmp);
						} else {
							hashTmp.removeAll(comprovacioRendimentTmp.llistaRutes.get(indexOfRoute).getWaypoints());
							System.out.println("HashSet (havent-hi tret el waypoints de la ruta B) = " + hashTmp);
							correctWaypoints = true;
						}
					} else {
						System.out.println("ERROR: no existeix la ruta " + currentNumber + " en el sistema.");
					}
				}
			} else {
				System.out.println("ERROR: introduïr 2 paràmetres separats per espais en blanc. Has introduït "
						+ numbers.length + " paràmetres");
			}
		}
	}

	public static void crearSetOrdenatDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		TreeSet<Ruta_Dades> setTmp = new TreeSet<Ruta_Dades>();
		// Loop all the routes and add them to the tree+
		for (Ruta_Dades route : comprovacioRendimentTmp.llistaRutes) {
			setTmp.add(route);
		}
		// And now loop all the routes in the tree and print info
		for (Ruta_Dades route : setTmp) {
			System.out.println("ID " + route.getId() + ": " + route.getNom() + ": waypoints" + route.getWaypoints());
		}
	}

	public static void crearLinkedHashMapDeRutes(ComprovacioRendiment comprovacioRendimentTmp) {
		LinkedHashMap<Integer, Ruta_Dades> hashTmp = new LinkedHashMap<Integer, Ruta_Dades>();
		long timeIterator, timeEntries, timeForEach;
		// Insert the content of the hashset to the hashmap
		for (int i = 0; i < comprovacioRendimentTmp.llistaRutes.size(); i++) {
			hashTmp.put(i, comprovacioRendimentTmp.llistaRutes.get(i));
		}
		// Insert the hash map entries to a set
		Set hashToSet = hashTmp.entrySet();
		// Get the iterator
		Iterator it = hashToSet.iterator();
		// Get the current nano time
		timeIterator = System.nanoTime();
		System.out.println("1a forma de visualitzar el contingut del map (map --> set + iterador del set):");
		// Display the info of each routes
		while(it.hasNext()) {
			Map.Entry entry = (Map.Entry)it.next();
			System.out.println("Clau del map = " + entry.getKey() + ":");
			System.out.println(entry.getValue());
			
		}
		timeIterator = System.nanoTime() - timeIterator;
		
		// Now iterate the hash map with an iterator of the keys
		Set<Integer> keys = hashTmp.keySet();
		it = keys.iterator();
		System.out.println("\n2a forma de visualitzar el contingut del map (iterator de les claus del map):");
		timeEntries = System.nanoTime();
		while (it.hasNext()) {
			Object key = it.next();
			System.out.println("Clau del map = " + key + ":");
			System.out.println(hashTmp.get(key));
		}
		timeEntries = System.nanoTime() - timeEntries;
		
		// Now iterate with for each
		System.out.println("\n3a forma de visualitzar el contingut del map (for-each del map --> set):");
		timeForEach = System.nanoTime();
		for (Map.Entry entry : hashTmp.entrySet()) {
		    Object key = entry.getKey();
		    Object route = entry.getValue();
		    System.out.println("Clau del map = " + key + ":");
		    System.out.println(route);
		}
		timeForEach = System.nanoTime() - timeForEach;
		
		// Print the time wasted for each method
		timeIterator = TimeUnit.MILLISECONDS.convert(timeIterator, TimeUnit.NANOSECONDS);
		timeEntries = TimeUnit.MILLISECONDS.convert(timeEntries, TimeUnit.NANOSECONDS);
		timeForEach = TimeUnit.MILLISECONDS.convert(timeForEach, TimeUnit.NANOSECONDS);
		System.out.println("\nTEMPS PER A 1a FORMA (map --> set + iterador del set):" + timeIterator);
		System.out.println("TEMPS PER A 1a FORMA (iterator de les claus del map):" + timeEntries);
		System.out.println("TEMPS PER A 1a FORMA (for-each del map --> set):" + timeForEach);
		
		// Copy the content of the hashMap to the one in the object sent by parameter
		comprovacioRendimentTmp.mapaLinkedDeRutes = hashTmp;
	}
	
	public static void visualitzarRutesDelMapAmbUnWaypointConcret(ComprovacioRendiment comprovacioRendimentTmp) {
		boolean waypointIsNotCorrect = true;
		int number;
		String waypoint;
		Scanner s = new Scanner(System.in);
		// Ask to the user the waypoint while the waypoint is not correct
		while (waypointIsNotCorrect) {
			System.out.print("Escriu el nº del waypoint que vols buscar: ");
			waypoint = s.nextLine();
			// Parse the waypoint to integer if it can be read as one
			if (Cadena.stringIsInt(waypoint)) {
				number = Integer.parseInt(waypoint);
				// Iterate the content of the map and check if the route contains the waypoint
				Set<Integer> keys = comprovacioRendimentTmp.mapaLinkedDeRutes.keySet();
				Iterator it = keys.iterator();
				System.out.println("RUTES QUE CONTENEN EL WAYPOINT:" + number);
				while (it.hasNext()) {
					Object key = it.next();
					// Check if the route contains the object
					if (comprovacioRendimentTmp.mapaLinkedDeRutes.get(key).getWaypoints().contains(number)) {
						System.out.println("Clau del map = " + key + ":");
						System.out.println(comprovacioRendimentTmp.mapaLinkedDeRutes.get(key));
					}					
				}
				waypointIsNotCorrect = false;
			} else {
				System.out.println(
						"ERROR: has introduït " + waypoint + " com a ruta. Els ID de les rutes són enters.");
			}
			
		}
	}
	
	public static void esborrarRutesDelMapAmbUnWaypointConcret(ComprovacioRendiment comprovacioRendimentTmp) {
		boolean waypointIsNotCorrect = true;
		int number;
		String waypoint;
		// Create a new LinkedHashMap to avoid remove problems
		LinkedHashMap<Integer, Ruta_Dades> hashTmp = new LinkedHashMap<Integer, Ruta_Dades>();
		Scanner s = new Scanner(System.in);
		// Ask to the user the waypoint while the waypoint is not correct
		while (waypointIsNotCorrect) {
			System.out.print("Escriu el nº del waypoint que vols buscar: ");
			waypoint = s.nextLine();
			// Parse the waypoint to integer if it can be read as one
			if (Cadena.stringIsInt(waypoint)) {
				number = Integer.parseInt(waypoint);
				// Iterate the content of the map and check if the route contains the waypoint
				Set<Integer> keys = comprovacioRendimentTmp.mapaLinkedDeRutes.keySet();
				Iterator it = keys.iterator();
				System.out.println("RUTES ESBORRADES QUE CONTENEN EL WAYPOINT:" + number);
				while (it.hasNext()) {
					Object key = it.next();
					// Check if the route contains the object
					if (comprovacioRendimentTmp.mapaLinkedDeRutes.get(key).getWaypoints().contains(number)) {
						System.out.println("Clau del map = " + key + ":");
						System.out.println(comprovacioRendimentTmp.mapaLinkedDeRutes.get(key));
						hashTmp.put((Integer) key, comprovacioRendimentTmp.mapaLinkedDeRutes.get(key));
					}					
				}
				waypointIsNotCorrect = false;
			} else {
				System.out.println(
						"ERROR: has introduït " + waypoint + " com a ruta. Els ID de les rutes són enters.");
			}
		}
		// And now remove all the elements in the hash in the hash in the object
		for (Map.Entry entry : hashTmp.entrySet()) {
		    Object key = entry.getKey();
		    Object route = entry.getValue();
		    comprovacioRendimentTmp.mapaLinkedDeRutes.remove(key, route);
		}
	}
	
	public static void visualitzarUnaRutaDelMap(ComprovacioRendiment comprovacioRendimentTmp) {
		boolean idIsNotCorrect = true;
		int number;
		String id;
		Scanner s = new Scanner(System.in);
		// Ask to the user the waypoint while the waypoint is not correct
		while (idIsNotCorrect) {
			System.out.print("Escriu el ID de la ruta que vols buscar: ");
			id = s.nextLine();
			// Parse the waypoint to integer if it can be read as one
			if (Cadena.stringIsInt(id)) {
				number = Integer.parseInt(id);
				// Iterate the content of the map and check if the route contains the waypoint
				Set<Integer> keys = comprovacioRendimentTmp.mapaLinkedDeRutes.keySet();
				Iterator it = keys.iterator();
				while (it.hasNext()) {
					Object key = it.next();
					// Check if the route contains the object
					if (comprovacioRendimentTmp.mapaLinkedDeRutes.get(key).getId() == number) {
						System.out.println(comprovacioRendimentTmp.mapaLinkedDeRutes.get(key));
						break;
					}					
				}
				idIsNotCorrect = false;
			} else {
				System.out.println(
						"ERROR: has introduït " + id + " com a ruta. Els ID de les rutes són enters.");
			}
			
		}
	}
	
	public static void ordenarRutesMapPerID(ComprovacioRendiment comprovacioRendimentTmp) {
		// Create the temporary TreeMap
		TreeMap tm = new TreeMap();
		// Iterate and insert the routes in the TreeMap
		for (Map.Entry entry : comprovacioRendimentTmp.mapaLinkedDeRutes.entrySet()) {
		    Object key = entry.getKey();
		    Object route = entry.getValue();
		    tm.put(comprovacioRendimentTmp.mapaLinkedDeRutes.get(key).getId(), route);
		}
		// Now iterate the TreeMap and see if the content is sorted by the id
		Set tmpSet = tm.entrySet();
		Iterator it = tmpSet.iterator();
		while(it.hasNext()) {
			Map.Entry me = (Map.Entry)it.next();
			System.out.println(me.getKey() + ": " + me.getValue());
		}
	}
	
	// No vaig saber fer l'últim
	public static void ordenarRutesMapPerWaypointsAndID(ComprovacioRendiment comprovacioRendimentTmp) {
		// Create the temporary TreeMap
		TreeMap<Integer, Ruta_Dades> tm = new TreeMap<Integer, Ruta_Dades>();
		// Iterate and insert the routes in the TreeMap
		for (Map.Entry entry : comprovacioRendimentTmp.mapaLinkedDeRutes.entrySet()) {
		    Object key = entry.getKey();
		    tm.put(comprovacioRendimentTmp.mapaLinkedDeRutes.get(key).getId(), comprovacioRendimentTmp.mapaLinkedDeRutes.get(key));
		}
		// Now iterate the TreeMap and see if the content is sorted by the id
		Set tmpSet = tm.entrySet();
		Iterator it = tmpSet.iterator();
		while(it.hasNext()) {
			Map.Entry me = (Map.Entry)it.next();
			System.out.println(me.getKey() + ": " + me.getValue());
		}
	}
}