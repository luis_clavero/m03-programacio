package com.daw.ClaveroLuis.krona;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.text.Collator;
import java.time.LocalDateTime;
import Llibreries.Cadena;
import Llibreries.Data;
import Llibreries.TipusWaypoint;

public class Waypoint {

	public static ComprovacioRendiment inicialitzarComprovacioRendiment() {
		// Instance the object and return it
		ComprovacioRendiment comprovacioRendimentTmp = new ComprovacioRendiment();
		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment comprovarRendimentInicialitzacio(int numObjACrear,
			ComprovacioRendiment comprovacioRendimentTmp) {
		int i;
		// Insert all the objects into the ArrayList and calculate the time wasted
		long initialTime = System.nanoTime();
		for (i = 0; i < numObjACrear; i++) {
			Waypoint_Dades waypoint = new Waypoint_Dades(comprovacioRendimentTmp.waypoint);
			comprovacioRendimentTmp.llistaArrayList.add(waypoint);
		}
		Long finalTime = System.nanoTime() - initialTime;
		finalTime = TimeUnit.MILLISECONDS.convert(finalTime, TimeUnit.NANOSECONDS);
		System.out.println("Temps per a insertar " + numObjACrear + " waypoints en l'ArrayList: " + finalTime);
		// Do the same with the LinkedList
		initialTime = System.nanoTime();
		for (i = 0; i < numObjACrear; i++) {
			Waypoint_Dades waypoint = new Waypoint_Dades(comprovacioRendimentTmp.waypoint);
			comprovacioRendimentTmp.llistaLinkedList.add(waypoint);
		}
		finalTime = System.nanoTime() - initialTime;
		finalTime = TimeUnit.MILLISECONDS.convert(finalTime, TimeUnit.NANOSECONDS);
		System.out.println("Temps per a insertar " + numObjACrear + " waypoints en el LinkedList: " + finalTime);
		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment comprovarRendimentInsercio(ComprovacioRendiment comprovacioRendimentTmp) {

		int size = comprovacioRendimentTmp.llistaArrayList.size();
		System.out.println("llistaArrayList.size(): " + size + ", meitatLlista: " + (size / 2));

		// Calculate the time that spends the program inserting 1 value to the first
		// position
		long initialTime = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(0, comprovacioRendimentTmp.waypoint);
		long finalTime = System.nanoTime() - initialTime;
		System.out.println("Temps per a insertar 1 waypoint en la 1a posició de l'ArrayList: " + finalTime);

		// Now do the same with the linked list
		initialTime = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(0, comprovacioRendimentTmp.waypoint);
		finalTime = System.nanoTime() - initialTime;
		System.out.println("Temps per a insertar 1 waypoint en la 1a posició del LinkedList: " + finalTime);
		System.out.println("------------");

		// Calculate the time that spends the program inserting 1 value to the middle
		// position
		initialTime = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(size / 2, comprovacioRendimentTmp.waypoint);
		finalTime = System.nanoTime() - initialTime;
		System.out.println(
				"Temps per a insertar 1 waypoint al mig (pos. " + (size / 2) + ") de l'ArrayList: " + finalTime);

		initialTime = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(size / 2, comprovacioRendimentTmp.waypoint);
		finalTime = System.nanoTime() - initialTime;
		System.out.println(
				"Temps per a insertar 1 waypoint al mig (pos. " + (size / 2) + ") del LinkedList: " + finalTime);
		System.out.println("------------");

		// Calculate the time that spends the program inserting 1 value to the last
		// position
		initialTime = System.nanoTime();
		comprovacioRendimentTmp.llistaArrayList.add(comprovacioRendimentTmp.waypoint);
		finalTime = System.nanoTime() - initialTime;
		System.out.println("Temps per a insertar 1 waypoint al final de l'ArrayList: " + finalTime);

		initialTime = System.nanoTime();
		comprovacioRendimentTmp.llistaLinkedList.add(comprovacioRendimentTmp.waypoint);
		finalTime = System.nanoTime() - initialTime;
		System.out.println("Temps per a insertar 1 waypoint al mig final del LinkedList: " + finalTime);
		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment modificarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {

		// First block
		System.out.println("---- APARTAT 1 ----");
		List<Integer> idsPerArrayList = new ArrayList<Integer>(comprovacioRendimentTmp.llistaArrayList.size());
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			idsPerArrayList.add(i);
		}
		System.out.println("S'ha inicialitzat la llista idsPerArrayList amb 10 elements.");
		int firstElement = idsPerArrayList.get(0);
		int lastElement = idsPerArrayList.get(idsPerArrayList.size() - 1);
		System.out.println("El 1r element té el valor: " + firstElement);
		System.out.println("L'últim element té el valor: " + lastElement);
		System.out.println();

		// Second Block
		System.out.println("---- APARTAT 2 ----");
		/*
		 * Bàsicament per a solucionar el problema de referències amb Java he fet un
		 * constructor nou de Waypoint_Dades que accepta un objecte i per tant seran
		 * objectes diferents.
		 */
		System.out.println(comprovacioRendimentTmp.llistaArrayList);
		for (Integer num : idsPerArrayList) {
			int firstId = comprovacioRendimentTmp.llistaArrayList.get(0).getId();
			System.out.println(
					"ABANS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get(" + num + ").getId(): " + firstId);
			comprovacioRendimentTmp.llistaArrayList.get(num).setId(num);
			System.out.println(
					"DESPRÉS DEL CANVI: comprovacioRendimentTmp.llistaArrayList.get(" + num + ").getId(): " + num);
			System.out.println();
		}

		// Third block
		System.out.println("---- APARTAT 3 ----");
		// Print the info of each item in the list with for loop
		System.out.println("---- APARTAT 3.1 (bucle for) ----");
		for (Integer num : idsPerArrayList) {
			int id = comprovacioRendimentTmp.llistaArrayList.get(num).getId();
			String name = comprovacioRendimentTmp.llistaArrayList.get(num).getNom();
			System.out.println("ID = " + id + ", nom = " + name);
		}
		System.out.println();
		System.out.println("---- APARTAT 3.2 (Iterator) ----");
		// Print the info of each item in the list with Iterator
		Iterator<com.daw.ClaveroLuis.krona.Waypoint_Dades> it = comprovacioRendimentTmp.llistaArrayList.iterator();
		while (it.hasNext()) {
			Waypoint_Dades waypoint = it.next();
			int id = waypoint.getId();
			String name = waypoint.getNom();
			System.out.println("ID = " + id + ", nom = " + name);
		}
		System.out.println();

		// Fourth block
		System.out.println("---- APARTAT 4 ----");
		// Show the size of the linked list
		int linkedSize = comprovacioRendimentTmp.llistaLinkedList.size();
		System.out.println(
				"Preparat per a esborrar el contingut de llistaLinkedList que té " + linkedSize + " elements.");
		// Erase the elements of the list and print the new size
		comprovacioRendimentTmp.llistaLinkedList.clear();
		linkedSize = comprovacioRendimentTmp.llistaLinkedList.size();
		System.out.println("Esborrada. Ara llistaLinkedList té " + linkedSize + " elements.");
		// Copy the elements in the arraylist into the linked one
		for (Waypoint_Dades waypoint : comprovacioRendimentTmp.llistaArrayList) {
			comprovacioRendimentTmp.llistaLinkedList.add(waypoint);
		}
		linkedSize = comprovacioRendimentTmp.llistaLinkedList.size();
		System.out.println("Copiats els elements de la llistaArrayList en llistaLinkedList que ara té " + linkedSize
				+ " elements");
		System.out.println();

		// Fifth block
		System.out.println("---- APARTAT 5.1 (bucle for) ----");
		// Change the name of the waypoints with id greater than 5
		for (Integer num : idsPerArrayList) {
			Waypoint_Dades waypoint = comprovacioRendimentTmp.llistaArrayList.get(num);
			if (waypoint.getId() > 5) {
				System.out.println("Modificat el waypoint amb id = " + num);
				waypoint.setNom("Òrbita de Mart");
			}
		}
		System.out.println();

		// Print the objects to see which changed
		System.out.println("---- APARTAT 5.1 (comprovació) ----");
		for (int i = 0; i < linkedSize; i++) {
			int id = comprovacioRendimentTmp.llistaArrayList.get(i).getId();
			String name = comprovacioRendimentTmp.llistaArrayList.get(i).getNom();
			System.out.println("ID = " + id + ", nom = " + name);
		}
		System.out.println();

		System.out.println("---- APARTAT 5.2 (Iterator) ----");
		// Change the name of the waypoints with id greater than 5
		it = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (it.hasNext()) {
			Waypoint_Dades waypoint = it.next();
			if (waypoint.getId() < 5) {
				System.out.println("Modificat el waypoint amb id = " + waypoint.getId());
				waypoint.setNom("Punt Lagrange entre la Terra i la LLuna");
			}
		}
		System.out.println();

		System.out.println("---- APARTAT 5.2 (comprovació) ----");
		// Print the objects to see which changed
		it = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (it.hasNext()) {
			Waypoint_Dades waypoint = it.next();
			int id = waypoint.getId();
			String name = waypoint.getNom();
			System.out.println("ID = " + id + ", nom = " + name);
		}
		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment esborrarWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {

		// First block
		System.out.println("---- APARTAT 1 ----");
		// Create the number list
		List<Integer> idsPerArrayList = new ArrayList<Integer>(comprovacioRendimentTmp.llistaArrayList.size());
		for (int i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			idsPerArrayList.add(i);
		}
		// Delete the objects with id lesser than 6
		for (Integer num : idsPerArrayList) {
			Waypoint_Dades waypoint = comprovacioRendimentTmp.llistaArrayList.get(num);
			if (waypoint.getId() < 6) {
				// Es poden esborrar aquests objectes degut a que no estem accedint directament
				// a ells
				comprovacioRendimentTmp.llistaArrayList.remove(num);
			}
		}
		System.out.println();

		// Second block
		System.out.println("---- APARTAT 2 (Iterator) ----");
		Iterator<com.daw.ClaveroLuis.krona.Waypoint_Dades> it = comprovacioRendimentTmp.llistaLinkedList.iterator();
		// Delete objects with id greater than 4
		while (it.hasNext()) {
			Waypoint_Dades waypoint = it.next();
			if (waypoint.getId() > 4) {
				it.remove();
			}
		}
		System.out.println();

		System.out.println("---- APARTAT 2 (comprovació) ----");
		// Print the objects to see which changed
		it = comprovacioRendimentTmp.llistaLinkedList.iterator();
		while (it.hasNext()) {
			Waypoint_Dades waypoint = it.next();
			int id = waypoint.getId();
			String name = waypoint.getNom();
			System.out.println("ID = " + id + ", nom = " + name);
		}
		System.out.println();

		// Third block
		System.out.println("---- APARTAT 3 (listIterator) ----");
		ListIterator<com.daw.ClaveroLuis.krona.Waypoint_Dades> listIt = comprovacioRendimentTmp.llistaLinkedList
				.listIterator();
		// Delete the element with id 2
		while (listIt.hasNext()) {
			Waypoint_Dades waypoint = listIt.next();
			if (waypoint.getId() == 2) {
				System.out.println("Esborrat el waypoint amb id " + waypoint.getId());
				listIt.remove();
			}
		}
		System.out.println();

		System.out.println("---- APARTAT 3 (comprovacio) ----");
		// Print the objects to see which changed
		while (listIt.hasPrevious()) {
			Waypoint_Dades waypoint = listIt.previous();
			int id = waypoint.getId();
			String name = waypoint.getNom();
			System.out.println("ID = " + id + ", nom = " + name);
		}

		return comprovacioRendimentTmp;
	}

	public static ComprovacioRendiment modificarCoordenadesINomDeWaypoints(
			ComprovacioRendiment comprovacioRendimentTmp) {
		int i, j, stringCoordinatesLength;
		String name, newName, coordinates, stringNewCoordinates;
		String[] newStringArrayCoordinates;
		int[] newIntArrayCoordinates;
		// Create a scanner to ask for names and coordinates
		Scanner s = new Scanner(System.in);
		for (i = 0; i < comprovacioRendimentTmp.llistaArrayList.size(); i++) {
			Waypoint_Dades waypoint = comprovacioRendimentTmp.llistaArrayList.get(i);
			if (waypoint.getId() % 2 == 0) {
				// Print the info of the waypoint if it's ID is even
				System.out.println("------ Modificar el waypoint amb id = " + waypoint.getId() + " ------");
				name = waypoint.getNom();
				System.out.println("Nom actual: " + name);
				// Ask to the user the new name and the new coordinates
				System.out.print("Nom nou: ");
				newName = s.nextLine();
				// Set the new name to the object
				waypoint.setNom(newName);
				// Show the current coordinates and ask for the new ones
				// Format the coordinates in a proper way
				coordinates = waypoint.getCoordenades()[0] + " " + waypoint.getCoordenades()[1] + " "
						+ waypoint.getCoordenades()[2];
				System.out.println("Coordenades actuals: " + coordinates);
				System.out.print("Coordenades noves (format: 1 13 7): ");
				stringNewCoordinates = s.nextLine();
				stringCoordinatesLength = stringNewCoordinates.split(" ").length;
				// Get the length of the coordinates sent
				stringCoordinatesLength = stringNewCoordinates.split(" ").length;
				// If the length is not 3 means there's more than 3 coordinates and we have to
				// ask for another ones
				// Initialize the array so it doesn't show error
				newIntArrayCoordinates = new int[waypoint.getCoordenades().length];
				while (stringCoordinatesLength != 3) {
					// Adding this condition because the error sent by a not correct value assigns
					// -1 to the variable
					if (stringCoordinatesLength != -1) {
						System.out.println("ERROR: introduïr 3 paràmetres separats per 1 espai en blanc. Has introduït "
								+ stringCoordinatesLength + " paràmetres.");
					}
					System.out.println("Coordenades actuals: " + coordinates);
					System.out.print("Coordenades noves (format: 1 13 7): ");
					stringNewCoordinates = s.nextLine();
					stringCoordinatesLength = stringNewCoordinates.split(" ").length;
					// Check if the length is 3 and check all cordinates are integers
					if (stringCoordinatesLength == 3) {
						// Create the integer array to append the coordinates
						newIntArrayCoordinates = new int[stringCoordinatesLength];
						// Create a String array with the new coordinates
						newStringArrayCoordinates = stringNewCoordinates.split(" ");
						// Now check if the coordinates are integers and not anything else
						for (j = 0; j < stringCoordinatesLength; j++) {
							if (Cadena.stringIsInt(newStringArrayCoordinates[j])) {
								// If it's a number add it to the array
								newIntArrayCoordinates[j] = Integer.parseInt(newStringArrayCoordinates[j]);
							} else {
								// If it's not a number show an error
								System.out.println("ERROR: coordenada " + newStringArrayCoordinates[j] + " no vàlida.");
								stringCoordinatesLength = -1;
								break;
							}
						}
					}
				}
				waypoint.setCoordenades(newIntArrayCoordinates);
				System.out.println();
			}
		}
		return comprovacioRendimentTmp;
	}

	public static void visualitzarWaypointsOrdenats(ComprovacioRendiment comprovacioRendimentTmp) {
		// Sort the array list
		Collections.sort(comprovacioRendimentTmp.llistaArrayList);
		// And now print each one of the objects
		for (Waypoint_Dades waypoint : comprovacioRendimentTmp.llistaArrayList) {
			System.out.println(waypoint);
		}
	}

	public static ComprovacioRendiment inicialitzarDadesWaypoints(ComprovacioRendiment comprovacioRendimentTmp) {
		// Check if the list is void and if it's not clear it
		if (comprovacioRendimentTmp.llistaWaypoints.size() != 0) {
			comprovacioRendimentTmp.llistaWaypoints.clear();
		}
		Waypoint_Dades waypoint0 = new Waypoint_Dades(0, "Òrbita de la Terra", new int[] {0,0,0}, true, LocalDateTime.parse("21-10-2020 01:10", Data.PATTERN), null, LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN), 0);
		Waypoint_Dades waypoint1 = new Waypoint_Dades(1, "Punt Lagrange Terra-LLuna", new int[] {1,1,1}, true, LocalDateTime.parse("21-10-2020 01:00", Data.PATTERN), null, LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN),6);
		Waypoint_Dades waypoint2 = new Waypoint_Dades(2, "Òrbita de la LLuna", new int[] {2,2,2}, true, LocalDateTime.parse("21-10-2020 00:50", Data.PATTERN), null, LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN),1);
		Waypoint_Dades waypoint3 = new Waypoint_Dades(3, "Òrbita de Mart", new int[] {3,3,3}, true, LocalDateTime.parse("21-10-2020 00:40", Data.PATTERN), null, LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN),0);
		Waypoint_Dades waypoint4 = new Waypoint_Dades(4, "Òrbita de Júpiter", new int[] {4,4,4}, true, LocalDateTime.parse("21-10-2020 00:30", Data.PATTERN), null, LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN),0);
		Waypoint_Dades waypoint5 = new Waypoint_Dades(5, "Punt Lagrange Júpiter-Europa", new int[] {5,5,5}, true, LocalDateTime.parse("21-10-2020 00:20", Data.PATTERN), null, LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN),6);
		Waypoint_Dades waypoint6 = new Waypoint_Dades(6, "Òrbita de Europa", new int[] {6,6,6}, true, LocalDateTime.parse("21-10-2020 00:10", Data.PATTERN), null, LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN),0);
		Waypoint_Dades waypoint7 = new  Waypoint_Dades(7, "Òrbita de Venus", new int[] {7,7,7}, true, LocalDateTime.parse("21-10-2020 00:01", Data.PATTERN), null, LocalDateTime.parse("22-10-2020 23:55", Data.PATTERN),0);
		
		comprovacioRendimentTmp.llistaWaypoints.add(waypoint0);
		comprovacioRendimentTmp.llistaWaypoints.add(waypoint1);
		comprovacioRendimentTmp.llistaWaypoints.add(waypoint2);
		comprovacioRendimentTmp.llistaWaypoints.add(waypoint3);
		comprovacioRendimentTmp.llistaWaypoints.add(waypoint4);
		comprovacioRendimentTmp.llistaWaypoints.add(waypoint5);
		comprovacioRendimentTmp.llistaWaypoints.add(waypoint6);
		comprovacioRendimentTmp.llistaWaypoints.add(waypoint7);
		return comprovacioRendimentTmp;
	}
	
	public static ComprovacioRendiment nouWaypoint(ComprovacioRendiment comprovacioRendimentTmp) {
		int greatestId = 0, i = 0, wType;
		int[] coordinatesToInteger = new int [3];
		String wName, wCoordinates, wActive, wAnulationData;
		String[] coordinates;
		boolean wrongAnswer = true, checker = false, wActiveToBoolean;
		Scanner s = new Scanner(System.in);
		// Look for the greatest id
		for (Waypoint_Dades wp : comprovacioRendimentTmp.llistaWaypoints) {
			if (wp.getId() > greatestId) {
				greatestId = wp.getId();
			}
		}
		// Sum 1 to the greatest id
		greatestId += 1;
		// Ask the name of the next waypoint
		System.out.print("Nom del waypoint: ");
		wName = s.nextLine();
		while (wrongAnswer) {
			System.out.print("Coordenades (format 1 13 7): ");
			wCoordinates = s.nextLine();
			coordinates = wCoordinates.split(" ");
			// Check if the length is 3
			if (coordinates.length != 3) {
				System.out.println("ERROR: introduïr 3 paràmetres separats per 1 espai en blanc. Has introduït "
						+ coordinates.length + " paràmetres.");
			} else {
				// Check if each one of them are coordinates and add them to the array
				for (String coordinate : coordinates) {
					if (!Cadena.stringIsInt(coordinate)) {
						System.out.println("ERROR: coordenada " + coordinate + " no vàlida.");
						i = 0;
						break;
					} else {
						coordinatesToInteger[i] = Integer.parseInt(coordinate);
						i++;
					}
					checker = true;
				}
				// If the checker is true means we can go to the next iteration
				if (checker) {
					checker = false;
					while (wrongAnswer) {
						// Check if the user introduces "true" or "false"
						System.out.print("Actiu? (format: true|false): ");
						wActive = s.nextLine();
						Collator comparator = Collator.getInstance();
						comparator.setStrength(Collator.PRIMARY);
						if (comparator.compare(wActive, "true") == 0) {
							wActiveToBoolean = true;
							checker = true;
						} else if (comparator.compare(wActive, "false") == 0) {
							wActiveToBoolean = false;
							checker = true;
						} else {
							System.out.println("ERROR: actiu " + wActive + " no vàlid.");
						}
						if (checker) {
							checker = false;
							while (wrongAnswer) {
								// Check if the format is correct 
								System.out.print("Data anulació (DD-MM-AAAA): ");
								wAnulationData = s.nextLine();
								if (Data.esData(wAnulationData)) {
									checker = true;
								} else {
									System.out.println("ERROR: data d'anulació " + wAnulationData + " no vàlida.");
								}
								if (checker) {
									checker = false;
									System.out.println("Tipus de waypoints disponibles: ");
									for (i = 0; i < TipusWaypoint.TIPUS_WAYPOINT.length; i++) {
										System.out.println(i + ": " + TipusWaypoint.TIPUS_WAYPOINT[i]);
									}
									while (wrongAnswer) {
										System.out.print("Tipus de waypoint (format: 3): ");
										wType = s.nextInt();
										if (wType >= 0 && wType < TipusWaypoint.TIPUS_WAYPOINT.length) {
											checker = true;
										} else {
											System.out.println("ERROR: introduïr 1 número dels visualitzats en la llista anterior. El número " + wType + " no existeix.");
										}
										if (checker) {
											wrongAnswer = false;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return comprovacioRendimentTmp;
	}
	
	public static void waypointsVsTipus(ComprovacioRendiment comprovacioRendimentTmp) {
		int id, i;
		ArrayList<Waypoint_Dades> llistaA = new ArrayList<Waypoint_Dades>();
		ArrayList<Waypoint_Dades> llistaB = new ArrayList<Waypoint_Dades>();
		Scanner s = new Scanner(System.in);
		// Show to the user the types and ask him one of them
		System.out.println("Tipus de waypoints disponibles: ");
		for (i = 0; i < TipusWaypoint.TIPUS_WAYPOINT.length; i++) {
			System.out.println(i + ": " + TipusWaypoint.TIPUS_WAYPOINT[i]);
		}
		System.out.print("Tipus de waypoint (format: 3): ");
		id = s.nextInt();
		// Copy all the objects in the list to the new list
		for (Waypoint_Dades wp : comprovacioRendimentTmp.llistaWaypoints) {
			llistaA.add(wp);
		}
		// Now erase all the objects with the type id in the list A and add them to the list B
		for (i = 0; i < llistaA.size(); i++) {
			if (llistaA.get(i).getTipus() == id) {
				llistaB.add(llistaA.remove(i));
				System.out.println("Esborrat el waypoint amb id = " + llistaA.get(i).getId() + "del tipus " + id + ". Waypoint llistaA --> llistaB");
			}
		}
		// And for the last print the elements in list B
		System.out.println();
		System.out.println("LlistaB de waypoints del tipus " + id + " :");
		for (Waypoint_Dades wp : llistaB) {
			System.out.println("id " + wp.getId() + " " + wp.getNom() + ", de tipus " + id);
		}
	}
	
	public static void numWaypointsVsNom(ComprovacioRendiment comprovacioRendimentTmp) {
		HashSet<Waypoint_Dades> hashTmp = new HashSet<Waypoint_Dades>();
		HashMap<String, Integer> hashMapTpm = new HashMap<String, Integer>();
		int occurrences = 0, i;
		// Add the waypoints to the hash
		for (Waypoint_Dades wp : comprovacioRendimentTmp.llistaWaypoints) {
			hashTmp.add(wp);
		}
		// Iterate the hash and the waypoint types and add all the occurrences in a new map
		for (i = 0; i < TipusWaypoint.TIPUS_WAYPOINT.length; i++) {
			for (Waypoint_Dades wp : hashTmp) {
				// If the type is the same number as the i means it's the same type of waypoint
				if (wp.getTipus() == i) {
					occurrences++;
				}
			}
			// Add the values to the map
			if (occurrences != 0) {
				hashMapTpm.put(TipusWaypoint.TIPUS_WAYPOINT[i], occurrences);
				occurrences = 0;
			}
		}
		// Now iterate the map and print the info to the user
		Set set = hashMapTpm.entrySet();
		Iterator it = set.iterator();
		while (it.hasNext()) {
			Map.Entry me = (Map.Entry)it.next();
			System.out.println("Tipus " + me.getKey() + ": " + me.getValue() + " naus.");
		}
	}
	
	public static void trobarWaypointsVsNom(ComprovacioRendiment comprovacioRendimentTmp) {
		Scanner s = new Scanner(System.in);
		String[] words;
		// Create the comparator
		Collator comparator = Collator.getInstance();
		comparator.setStrength(Collator.SECONDARY);
		System.out.print("Paraula que vols buscar: ");
		String word = s.nextLine();
		// Iterate all the waypoints
		for (Waypoint_Dades wp : comprovacioRendimentTmp.llistaWaypoints) {
			// Split the string and iterate it 
			words = wp.getNom().split("-| ");
			for (String str : words) {
				// Compare if two strings are similar
				if (comparator.compare(str, word) == 0) {
					System.out.println("id " + wp.getId() + ": " + wp.getNom());
				}
			}
		}
	} 
	
	public static void ordenarWaypointsPerData(ComprovacioRendiment comprovacioRendimentTmp) {
		// Sort the data
		Collections.sort(comprovacioRendimentTmp.llistaWaypoints);
		// Print the data sorted
		for (Waypoint_Dades wp : comprovacioRendimentTmp.llistaWaypoints) {
			System.out.println("id " + wp.getId() + ": " + wp.getNom() + ", data d'alta: " + wp.getDataCreacio());
		}
	} 
}
