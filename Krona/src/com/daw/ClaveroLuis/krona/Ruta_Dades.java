package com.daw.ClaveroLuis.krona;

import java.time.LocalDateTime;
import java.util.ArrayList;

public class Ruta_Dades implements Comparable<Ruta_Dades> {
	private int id;                             	
	private String nom;
	private ArrayList<Integer> waypoints;       	
	private boolean actiu;                      	
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;             
	private LocalDateTime dataModificacio;
	
	public Ruta_Dades(int id, String nom, ArrayList<Integer> waypoints, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		super();
		this.id = id;
		this.nom = nom;
		this.waypoints = waypoints;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
	}
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public ArrayList<Integer> getWaypoints() {
		return waypoints;
	}

	public void setWaypoints(ArrayList<Integer> waypoints) {
		this.waypoints = waypoints;
	}

	public boolean isActiu() {
		return actiu;
	}

	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}

	public LocalDateTime getDataCreacio() {
		return dataCreacio;
	}

	public void setDataCreacio(LocalDateTime dataCreacio) {
		this.dataCreacio = dataCreacio;
	}

	public LocalDateTime getDataAnulacio() {
		return dataAnulacio;
	}

	public void setDataAnulacio(LocalDateTime dataAnulacio) {
		this.dataAnulacio = dataAnulacio;
	}

	public LocalDateTime getDataModificacio() {
		return dataModificacio;
	}

	public void setDataModificacio(LocalDateTime dataModificacio) {
		this.dataModificacio = dataModificacio;
	}

	@Override
	public String toString() {
		return "Dades de la ruta: " + 
				"\n\tid = " + this.id +
				"\n\tnom = " + this.nom +
				"\n\tactiu = " + this.actiu +
				"\n\tdataCreacio = " + this.dataCreacio +
				"\n\tdataAnulacio = " + this.dataAnulacio +
				"\n\tdataModificacio = " + this.dataModificacio +
				"\n\twaypoints = " + this.waypoints;
	}

	@Override
	public int compareTo(Ruta_Dades route) {
		if (this.getWaypoints().equals(route.getWaypoints())) {
			return 0;
		} else {
			return route.getId() - this.id;
		}
	}
}
