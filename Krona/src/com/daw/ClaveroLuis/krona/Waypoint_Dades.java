package com.daw.ClaveroLuis.krona;

import java.text.Collator;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Locale;

public class Waypoint_Dades implements Comparable<Waypoint_Dades>{
	private int id;                     
	private String nom;
	private int tipus;
	private int[] coordenades;
	private boolean actiu;              
	private LocalDateTime dataCreacio;
	private LocalDateTime dataAnulacio;      
	private LocalDateTime dataModificacio;
	private Waypoint_Dades waypoint;
	
	public Waypoint_Dades(int id, String nom, int[] coordenades, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio) {
		super();
		this.id = id;
		this.nom = nom;
		this.coordenades = coordenades;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
		this.tipus = tipus;
	}
	
	public Waypoint_Dades(int id, String nom, int[] coordenades, boolean actiu, LocalDateTime dataCreacio,
			LocalDateTime dataAnulacio, LocalDateTime dataModificacio, int tipus) {
		super();
		this.id = id;
		this.nom = nom;
		this.coordenades = coordenades;
		this.actiu = actiu;
		this.dataCreacio = dataCreacio;
		this.dataAnulacio = dataAnulacio;
		this.dataModificacio = dataModificacio;
		this.tipus = tipus;
	}
	
	/*
	 * Bàsicament per a solucionar el problema de referències amb Java he fet un constructor nou de Waypoint_Dades
	 * que accepta un objecte i per tant seran objectes diferents.
	 */
	public Waypoint_Dades(Waypoint_Dades waypoint) {
		super();
		this.id = waypoint.getId();
		this.nom = waypoint.getNom();
		this.coordenades = waypoint.getCoordenades();
		this.actiu = waypoint.isActiu();
		this.dataCreacio = waypoint.getDataCreacio();
		this.dataAnulacio = waypoint.getDataAnulacio();
		this.dataModificacio = waypoint.getDataModificacio();
	}
	
	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public int[] getCoordenades() {
		return coordenades;
	}



	public void setCoordenades(int[] coordenades) {
		this.coordenades = coordenades;
	}



	public boolean isActiu() {
		return actiu;
	}



	public void setActiu(boolean actiu) {
		this.actiu = actiu;
	}



	public LocalDateTime getDataCreacio() {
		return dataCreacio;
	}



	public void setDataCreacio(LocalDateTime dataCreacio) {
		this.dataCreacio = dataCreacio;
	}



	public LocalDateTime getDataAnulacio() {
		return dataAnulacio;
	}



	public void setDataAnulacio(LocalDateTime dataAnulacio) {
		this.dataAnulacio = dataAnulacio;
	}



	public LocalDateTime getDataModificacio() {
		return dataModificacio;
	}



	public void setDataModificacio(LocalDateTime dataModificacio) {
		this.dataModificacio = dataModificacio;
	}


	public int getTipus() {
		return tipus;
	}

	public void setTipus(int tipus) {
		this.tipus = tipus;
	}

	@Override
	public String toString() {
		String dataCreacio, dataAnulacio, dataModificacio;
		// Format and check that dates are not null
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm");
		if (this.dataCreacio == null) {
			dataCreacio = "NULL";
		} else {
			dataCreacio = this.dataCreacio.format(formatter);
		}
		if (this.dataAnulacio == null) {
			dataAnulacio = "NULL";
		} else {
			dataAnulacio = this.dataAnulacio.format(formatter);
		}
		if (this.dataModificacio == null) {
			dataModificacio = "NULL";
		} else {
			dataModificacio = this.dataModificacio.format(formatter);
		}
		return "WAYPOINT " + this.id + ":" + "\n" +
				"\tnom = " + this.nom + "\n" + 
				"\tcoordenades (x, y, z) = " + this.coordenades + "\n" +
				"\tactiu = " + this.actiu + "\n" +
				"\tdataCreacio" + dataCreacio + "\n" +
				"\tdataAnulacio" + dataAnulacio + "\n" +
				"\tdataModificacio" + dataModificacio + "\n"
				;
	}

	/*
	@Override
	public int compareTo(Waypoint_Dades waypoint) {
		boolean areTheSameCoordinates = true;
		int i;
		double thisDistance = 0, otherDistance = 0;
		// Check if the coordinates are the same 
		for (i = 0; i < this.coordenades.length; i++) {
			if (this.coordenades[i] != waypoint.getCoordenades()[i]) {
				areTheSameCoordinates = false;
				break;
			}
		}
		if (areTheSameCoordinates) {
			// Set the collator and it's strength and compare the strings
			Collator collator = Collator.getInstance(new Locale("es"));
			collator.setStrength(Collator.PRIMARY);
			return collator.compare(this.nom, waypoint.nom);
		} else {
			// Compare the coordinates and calculate it's power
			for (i = 0; i < this.coordenades.length; i++) {
				thisDistance += Math.pow(this.coordenades[i], 2);
				otherDistance += Math.pow(waypoint.coordenades[i], 2);
			}
			return (int)(thisDistance - otherDistance);
		}
	}*/
	@Override 
	public int compareTo(Waypoint_Dades waypoint) {
		return this.getDataCreacio().compareTo(waypoint.getDataCreacio());
	}
    
	
}
