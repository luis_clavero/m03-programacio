package exercici_reforç_de_1r;

import java.time.LocalDateTime;

public class Oficial extends Tripulant {
	
	// Oficial attributes
	
	private boolean serveiEnElPont;
	private String descripcioFeina;
	
	// Constructor 
	
	public Oficial(String id, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei,
			String descripcioFeina) {
		super(id, nom, actiu, dataAlta, departament, llocDeServei);
		this.serveiEnElPont = this.serveixEnElPont();
		this.descripcioFeina = descripcioFeina;
	}
	
	// Setters & Getters
	
	public String getDescripcioFeina() {
		return descripcioFeina;
	}

	public void setDescripcioFeina(String descripcioFeina) {
		this.descripcioFeina = descripcioFeina;
	}
	
	// Methods
	

	protected void ImprimirDadesTripulant() {
		System.out.println("Bandol: " + Tripulant.BANDOL);
		System.out.println("Id: " + this.id);
		System.out.println("Nom: " + this.nom);
		System.out.println("Actiu: " + this.actiu);
		System.out.println("Data d'alta: " + this.dataAlta);
		System.out.println("Departament: " + this.departament);
		System.out.println("Lloc de servei: " + this.getLlocDeServei());
		System.out.println("Serveix al pont? " + this.serveiEnElPont);
		System.out.println("Descripci� de la feina: " + this.descripcioFeina);
	}
	
	/**
	 * Check if the Tripulant works on the bridge.
	 * 
	 * @param void
	 * @return true if works on the bridge false otherwise
	 */
	private boolean serveixEnElPont() {
		if (this.getLlocDeServei() == 1) {
			return true;
		} else {
			return false;
		}
	}
	
	@Override
	protected void saludar() {
		System.out.println("Hola des de la subclasse Oficial");
	}

	@Override
	public String toString() {
		return "Oficial [serveiEnElPont=" + serveiEnElPont + ", descripcioFeina=" + descripcioFeina + ", id=" + id
				+ ", nom=" + nom + ", actiu=" + actiu + ", dataAlta=" + dataAlta + ", departament=" + departament + "]";
	}
}
