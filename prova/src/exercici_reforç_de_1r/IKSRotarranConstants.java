package exercici_reforç_de_1r;

public interface IKSRotarranConstants {
	
	// Constants
	
	public static final String[] LLOCS_DE_SERVEI = {"", "enginyeria", "cuina", "infermeria", "sala d'armes"};
	public static final String[] DEPARTAMENTS = {"", "comandament", "armes", "timó i navegació", "enginyeria", "ciència"};
}
