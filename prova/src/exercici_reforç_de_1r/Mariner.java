package exercici_reforç_de_1r;

import java.time.LocalDateTime;

public class Mariner extends Tripulant {
	
	// Mariner attributes
	
	private boolean serveiEnElPont;
	private String descripcioFeina;
	
	// Constructor 
	
	public Mariner(String id, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei,
			String descripcioFeina) {
		super(id, nom, actiu, dataAlta, departament, llocDeServei);
		this.serveiEnElPont = this.serveixEnElPont();
		this.descripcioFeina = descripcioFeina;
	}

	// Setters & Getters
	
	public String getDescripcioFeina() {
		return descripcioFeina;
	}

	public void setDescripcioFeina(String descripcioFeina) {
		this.descripcioFeina = descripcioFeina;
	}
	
	// Methods
	
	protected void ImprimirDadesTripulant() {
		System.out.println("DADES TRIPULANT:");
		System.out.println("\tBandol: " + Tripulant.BANDOL);
		System.out.println("\tID: " + this.id);
		System.out.println("\tNom: " + this.nom);
		System.out.println("\tActiu: " + this.actiu);
		System.out.println("\tDepartament (de la classe Tripulant): " + this.departament);
		System.out.println("\tDepartament (de la classe IKSRotarranConstants): " + IKSRotarranConstants.DEPARTAMENTS[this.departament]);
		System.out.println("\tLloc de servei (de la classe Tripulant): " + this.getLlocDeServei());
		System.out.println("\tLloc de servei (de la classe IKSRotarranConstants): " + IKSRotarranConstants.LLOCS_DE_SERVEI[this.departament]);
		System.out.println("\tDescripció de la feina: " + this.descripcioFeina);
		System.out.println("\tServeix al pont? " + this.serveiEnElPont);
		System.out.println("\tData d'alta: " + IKSRotarran.dateToString(this.dataAlta));
	}
	
	/**
	 * Check if the Tripulant works on the bridge.
	 * 
	 * @param void
	 * @return true if works on the bridge false otherwise
	 */
	private boolean serveixEnElPont() {
		if (this.getLlocDeServei() == 1) {
			return true;
		} else {
			return false;
		}
	}
	
}
