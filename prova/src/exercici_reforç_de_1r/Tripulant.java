package exercici_reforç_de_1r;

import java.time.LocalDateTime;

public abstract class Tripulant {
	
	// Tripulant attributes
	
	protected static final String BANDOL = "Imperi Klingon";
	protected String id;
	protected String nom;
	protected boolean actiu;		// Si el metge el dona de baixa a llavors valdr� false
	protected LocalDateTime dataAlta;	
	protected int departament;		/* Departament al qual pertany: comandament (1),
									armes (2), tim� i navegaci� (3), enginyeria (4) i ci�ncia (5) */
	
	private int llocDeServei;		/* Lloc en el qual serveix: pont (1), enginyeria (2), 
									cuina (3), infermeria (4) o sala d'armes (5). */
	
	// Constructor
	
	public Tripulant(String id, String nom, boolean actiu, LocalDateTime dataAlta, int departament, int llocDeServei) {
		super();
		this.id = id;
		this.nom = nom;
		this.actiu = actiu;
		this.dataAlta = dataAlta;
		this.departament = departament;
		this.llocDeServei = llocDeServei;
	}
	
	// Getters and setters
	
	public int getLlocDeServei() {
		return llocDeServei;
	}

	public void setLlocDeServei(int llocDeServei) {
		this.llocDeServei = llocDeServei;
	}
	
	// Methods

	protected abstract void ImprimirDadesTripulant();
	
	protected void saludar() {
		System.out.println("Hola des de la superclasse Tripulant");
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tripulant other = (Tripulant) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
