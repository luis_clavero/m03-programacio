
public interface Vehicle {
	
	void drive();
	void stop();
	void startEngine();
	void turnLeft();
	void goToITV();
}
