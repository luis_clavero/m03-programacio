import java.util.ArrayList;
import java.util.Iterator;

public class JavaCollections {

	public static void main(String[] args) {

		ArrayList<String> l1 = new ArrayList<String>();

		l1.add("Gat");
		l1.add("Jaguar");
		l1.add("Linx");
		l1.add("Pantera");
		// System.out.println(l1.get(0));
		// l1.set(0, "Gos");
		// System.out.println(l1.get(0));
		
		// Loop the ArrayList with a simple for

		System.out.println("Amb bucle for convencional:");
		for (int i = 0; i < l1.size(); i++) {
			System.out.println("Objecte " + i + ": " + l1.get(i));
		}
		
		// Loop the ArrayList with an Iterator
		
		System.out.println("\nAmb while i objecte Iterator:");
		Iterator<String> i = l1.iterator();
		int count = 0;
		while (i.hasNext()) {
			System.out.println("Objecte " + count + ": " + i.next());
			count++;
		}
		
		// Loop the ArrayList with for each structure
		
		System.out.println("\nAmb for each:");
		count = 0;
		for (String animal : l1) {
			System.out.println("Objecte " + count + ": " + animal);
			count++;
		}
		
		// Create a new Array and dump the ArrayList into it
		
		String s[] = new String[l1.size()];
		s = (String[]) l1.toArray(s);
		
		// Create some Car objects
		
		Car c1 = new Car("3 Series", 20000, "BMw");
		Car c2 = new Car("Model S", 35000, "Tesla");
		Car c3 = new Car("A6", 45000, "Audi");
		
		// Create a List and add the cars to it
		
		ArrayList<Car> cars = new ArrayList<Car>();
		cars.add(c1);
		cars.add(c2);
		cars.add(c3);
		
		// Print the information of each car
		
		count = 0;
		for (Car car : cars) {
			System.out.println("\nCar " + count + ": ");
			System.out.println("Brand: " + car.getBrand());
			System.out.println("Name: " + car.getName());
			System.out.println("Price (€): " + car.getPrice());
			count++;
		}
	}

}
