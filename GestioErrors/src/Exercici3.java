
public class Exercici3 {

	public static Exception divisio() {
		Exception e = new Exception("El divisor és 0");
		return e;
	}
	
	public static void main(String[] args) {
		try {
			System.out.println((6 / 0));
		} catch (ArithmeticException e) {
			System.out.println(divisio());
		}
	}

}
