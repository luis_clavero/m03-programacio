
public class Exercici5ValidarEdatException extends Exception {
	
	public Exercici5ValidarEdatException() {
		super("La edat ha de ser entre 0 i 100 anys");
	}
}
