
public class Persona {

	private int edat;
	
	public void setEdat(int edat) {
		if (edat < 0) {
			throw new IllegalArgumentException();
		} else {
			this.edat = edat;
		}
	}
	
	public void setEdat2(int edat) throws Exercici5ValidarEdatException {
		if (edat < 0 || edat > 100) {
			throw new Exercici5ValidarEdatException();
		} else {
			this.edat = edat;
		}
	}
	
	public static void main(String[] args) {
		try {
			Persona p = new Persona();
			p.setEdat(-1);
		} catch (IllegalArgumentException e) {
			System.out.println(e);
			System.out.println("L'edat introduïda és negativa!");
		}
		
		try {
			Persona p = new Persona();
			p.setEdat2(101);
		} catch (Exercici5ValidarEdatException e) {
			System.out.println(e);
			System.out.println("Exercici5ValidarEdatException capturada.");
		}
	}

}
