
public class Exercici1 {

	public static void funcio1() {
		System.out.println("S'executa la funció 1: ");
		funcio2();
		System.out.println("Fi de l'execució de la funció 1");
	}
	
	public static void funcio2() {
		try {
			System.out.println("S'executa la funció 2 i executem la funció 3: ");
			funcio3();
			System.out.println("Tornem a la funció 2 després d'executar la funció 3");
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("Excepció capturada a la funció 2");
		}
	}
	
	public static void funcio3() {
		System.out.println("S'executa la funció 3: ");
		try {
			int[] integers = {1, 2, 3, 4};
			System.out.println(integers[4]);
		} catch (ArrayIndexOutOfBoundsException e) {
			System.out.println(e);
		} finally {
			System.out.println("Final del programa!"); /* Aquesta línia de codi nomès s'executarà si la
			 											tractem de forma correcta l'excepció, encara que no es necessari */
		}
		
		try {
			int[] integers = {1, 2, 3, 4};
			System.out.println(integers[4]);
		} catch (Exception e) { /* Si capturem la excepció relacionada amb un String no s'executarà el codi del catch
									atès que no és la excepció correcta que "salta" però si capturem un Exception
									generic si que s'executarà */
			System.out.println(e.getMessage());
			System.out.println(e.getCause());
			e.printStackTrace();
		} finally { // Aquest codi s'executarà sempre, salti cap excepció o no, per tant sempre sortirà al final
			System.out.println("Final del programa!");
		}
		System.out.println("Fi de l'execució de la funció 3");
	}
	
	public static void main(String[] args) {
		System.out.println("Cridem la funció 1:");
		funcio1();
		System.out.println("Fi del main");
	}

}
