import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class Exercici2 {

	public static void file() throws IOException {
			FileOutputStream f = new FileOutputStream ("../docs/test.txt"); /* SecurityException nomès es dona quan no tenim accès
			per a escriure al fitxer, com no existeix tampoc saltarà aquesta excepció, en el cas d'existir si que podria passar */
			f.close();
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			file();
		} catch (FileNotFoundException e) {
			System.out.println("Capturada l’excepció FileNotFoundException: ");
			System.out.println(e);
		} catch (IOException e) {
			System.out.println("Capturada l’excepció IOException: ");
			System.out.println(e);
		}
	}

}
