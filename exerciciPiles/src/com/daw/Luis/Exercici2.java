package com.daw.Luis;

import java.util.ArrayDeque;
import java.util.Deque;

public class Exercici2 {

	/*
	 * I did a version of this method with Strings since i didn't know how to do it
	 * without them.
	 */
	public static Deque<String> returnDiamondsString(String diamondMine) {
		Deque<String> diamondDeque = new ArrayDeque<String>();
		// Get the size of the mine
		int i, size = diamondMine.length(), sizeCheck = diamondMine.length(), whiteDiamonds = 0, blackDiamonds = 0;
		for (i = 1; i < size; i++) {
			// Remove the first occurrence of a white diamond
			diamondMine = diamondMine.replaceFirst("<>", "");
			// If the length of the mine has changed means something has been replaced
			if (sizeCheck != diamondMine.length()) {
				sizeCheck = diamondMine.length();
				whiteDiamonds++;
			}
			// Remove the first occurrence of a black diamond and check the size
			// I don't know why but in order to remove the parenthesis you have to represent them as a regular expression
			diamondMine = diamondMine.replaceFirst("[(][)]", "");
			if (sizeCheck != diamondMine.length()) {
				sizeCheck = diamondMine.length();
				blackDiamonds++;
			}
		}
		// Add the white diamonds and the black diamonds to the list
		diamondDeque.addLast(whiteDiamonds + "b");
		diamondDeque.addLast(blackDiamonds + "n");
		return diamondDeque;
	}
	
	/*
	 * Version with Deque methods.
	 */
	public static String returnDiamondsDeque(String[] diamondMine) {
		Deque<String> diamondDeque = new ArrayDeque<String>();
		int whiteDiamonds = 0, blackDiamonds = 0;
		String diamonds = "";
		for (String diamondPiece : diamondMine)
			/* If the first value of the deque is a < and the current element of the array is a > 
			* delete it and add 1 to the white diamonds
			*/
			if (!diamondDeque.isEmpty() && diamondDeque.peekFirst().equals("<") && diamondPiece.equals(">")) {
				whiteDiamonds++;
				diamondDeque.pop();
			/* Now do the same checking the black diamonds "()" and if there's not any black diamonds, 
			* add it to the deque
			*/
			} else {
				if (!diamondDeque.isEmpty() && diamondDeque.peekFirst().equals("(") && diamondPiece.equals(")")) {
					blackDiamonds++;
					diamondDeque.pop();
				} else {
					diamondDeque.push(diamondPiece);
			}
		}
		diamonds += whiteDiamonds + "b";
		diamonds += " " + blackDiamonds + "n";
		return diamonds;
	}
}
