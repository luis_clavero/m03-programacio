package com.daw.Luis;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.NoSuchElementException;

public class Exercici3 {

	public static Deque<Character> checkMathExpression(String mathExpression) {
		Deque<Character> parenthesis = new ArrayDeque<Character>();
		Deque<Character> parenthesisStack = new ArrayDeque<Character>();
		int i, size;
		char currentChar;
		// Get the parenthesis of the expression
		for (i = 0; i < mathExpression.length(); i++) {
			currentChar = mathExpression.charAt(i);
			if (currentChar == '(' || currentChar == ')') {
				parenthesis.addLast(currentChar);
			}
		}
		// Now check each character of the parenthesis array
		size = parenthesis.size();
		for (i = 0; i < size; i++) {
			currentChar = parenthesis.pop();
			// If it's a "(" add it to the stack
			if (currentChar == '(') {
				parenthesisStack.addFirst(currentChar);
			// If it's a ")" remove 1 item from the stack
			} else if (currentChar == ')') {
				try {
					currentChar = parenthesisStack.pop();
				}
				catch(NoSuchElementException e) {
					System.out.println("Exception error.");
				}
			}
		}
		return parenthesisStack;
	}
}
