package com.daw.Luis;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Iterator;

public class Exercici4<T> {

	// Reverse the deque with a for iteration
	public Deque<T> reverseQueue(Deque<T> deque) {
		Deque<T> newDeque = new ArrayDeque<>();
		int size = deque.size(); 
		for (int i = 0; i < size; i++) {
			newDeque.push(deque.pop());
		}
		return newDeque;
	}
	
	// Reverse the deque with an Iterator
	public Deque<T> reverseQueueIterator(Deque<T> deque) {
		Deque<T> newDeque = new ArrayDeque<>();
		Iterator<T> it = deque.descendingIterator();
		while (it.hasNext()) {
			newDeque.add(it.next());
		}
		return newDeque;
	}
}
