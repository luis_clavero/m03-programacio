package com.daw.Luis;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.Deque;
import java.util.LinkedList;

public class Main {

	public static void main(String[] args) {
		
		// Try the first exercise
		Deque<Integer> integers = new ArrayDeque<Integer>();
		integers.addLast(0);
		integers.addLast(1);
		integers.addLast(2);
		integers.addLast(3);
		Exercici1 ex1 = new Exercici1();
		System.out.println("Reversed List: " + ex1.invertirPila(integers));
		
		// Try the second exercise
		String mine = "<>(<>)<(<)";
		System.out.println("Diamonds: " + Exercici2.returnDiamondsString(mine));
		
		String[] diamondMine2 = {"<", ">", "(", "<", ">", ")"};
		System.out.println("Diamonds: " + Exercici2.returnDiamondsDeque(diamondMine2));
		
		// Try the third exercise
		String mathExpression = "25+3*(1+2+(30*4))/2";
		System.out.println("Expressió matemàtica: " + Exercici3.checkMathExpression(mathExpression));
		
		// Try the fourth exercise
		Deque<String> cua1 = new LinkedList<String>();
        Deque<String> cua2 = new ArrayDeque<>();

        cua1.addAll(Arrays.asList("1","2","3","4"));
        cua2.addAll(Arrays.asList("1","2","3","4"));
        
        Exercici4 ex4 = new Exercici4();
        System.out.println("Revertir cua 1: " + ex4.reverseQueue(cua1));
        System.out.println("Revertir cua 2: " + ex4.reverseQueue(cua2));
        
        cua1.addAll(Arrays.asList("1","2","3","4"));
        cua2.addAll(Arrays.asList("1","2","3","4"));
        
        System.out.println("Revertir cua 1 Iterator: " + ex4.reverseQueueIterator(cua1));
        System.out.println("Revertir cua 2 Iterator: " + ex4.reverseQueueIterator(cua2));
	}

}
