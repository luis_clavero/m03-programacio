package com.daw.Luis;

import java.util.ArrayDeque;
import java.util.Deque;

public class Exercici1<T> {

	public Deque<T> invertirPila(Deque<T> deque) {
		Deque<T> newDeque = new ArrayDeque<>();
		int i, size = deque.size();
		T firstElement;
		// Get the first item and add it to the new list
		for (i = 0; i < size; i++) {
			firstElement = deque.removeFirst();
			newDeque.addFirst(firstElement);
		}
		return newDeque;
	}
}
